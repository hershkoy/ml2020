


python -m venv neurosteer
pip install --user virtualenv
.\neurosteer\Scripts\activate

pip install ipykernel spyder-kernels
python -m ipykernel install --user --name=neurosteer   
	=> store the envs at "%AppData%\Roaming\jupyter\kernels\"


pip install -f requirements.txt


======

for spyder
> python -m spyder_kernels.console

kernels are at:
%AppData%\Roaming\jupyter\runtime

> jupyter kernelspec list

stop the kernel using ctrl-break!! (in command line)

to connect to kernel in spyder: menu=>consoles=>connect to existing kernels