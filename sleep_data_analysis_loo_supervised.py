#!/usr/bin/env python
# coding: utf-8

# kernal: conda_tensorflow_36

# In[1]:


get_ipython().system('pip install kneed kneebow pandas==0.24.2 umap_learn')


# In[2]:


get_ipython().system('pip uninstall -y enum34 ')
get_ipython().system('pip install mlflow')


# In[4]:


#import tensorflow
#from tensorflow.python.client import device_lib
#print(device_lib.list_local_devices())


# In[5]:


import boto3

bucket_name = 'hezi-sagemaker'
INPUT_DIR = 'data'

cred = boto3.Session().get_credentials()
ACCESS_KEY = cred.access_key
SECRET_KEY = cred.secret_key

s3 = boto3.resource('s3', region_name='us-east-2')
bucket = s3.Bucket(bucket_name)

s3client = boto3.client('s3', 
            aws_access_key_id = ACCESS_KEY, 
            aws_secret_access_key = SECRET_KEY
           )


# In[6]:


for my_bucket_object in bucket.objects.all():
    print(my_bucket_object)


# In[7]:


get_ipython().system('rm -rf ml2020')
get_ipython().system('git clone https://hershkoy@bitbucket.org/hershkoy/ml2020.git')


# In[8]:


import pickle
import mlflow

import sklearn
from sklearn.preprocessing import FunctionTransformer
from sklearn.metrics import mean_squared_error,adjusted_rand_score

import umap.umap_ as umap

import os
import sys
import glob
import matplotlib.pyplot as plt
import numpy as np
import random
import itertools


sys.path.insert(0,os.path.join(os.getcwd(),"ml2020/code/lib"))

import skRPCA
import ClusterFinder
import Vizualization
import experiment_utils as utils


viz = Vizualization.Vizualization()


# In[9]:


#require credentials
response = s3client.get_object(Bucket=bucket_name,Key='neurosteer/data/sleep_data/df_features_test_env_unlabeled.pkl')
body = response['Body'].read()

df = pickle.loads(body)

ind = df.index
recs = ind.get_level_values(0)

sessionNames_list = df.index.levels[0]
print(sessionNames_list)


#%%

def all_but_first_column(X):
    return X[:, 1:]

experiment_name = "leave_one_out"
image_dir = "images_tmp"
dat_id = [0,1,2,3,4,5,6]

hypnogram_smoothing = 30

test_id = 2


cand = [[0,121],         
        [10,30,50],
        [10,16,24,30], #,36,42,50,100,200],
        [0.0, 0.1, 0.25, 0.5, 0.8, 0.99],
        ['euclidean'], #['euclidean','manhattan','chebyshev','minkowski'],
        [False,True]]

cand_list = list(itertools.product(*cand))
random.shuffle(cand_list)

base_n_rpca_components = 121
base_n_pca_components = 50
base_umap_n_neighbors = 30
base_umap_min_dist = 0.1
base_umap_metric = 'euclidean'
base_dbscan_eps = 0.4
base_dbscan_min_samples = 20


base_pipe = sklearn.pipeline.Pipeline(steps=[
    # ('scalar', StandardScaler()),
    ('rpca', skRPCA.skRPCA(n_components=base_n_rpca_components)),
    ('pca', utils.PCA(n_components=base_n_pca_components)),
    ('remove_dimension', FunctionTransformer(all_but_first_column)),
    ('reduce', umap.UMAP(n_neighbors=base_umap_n_neighbors, min_dist=base_umap_min_dist, metric=base_umap_metric,verbose=True)),
    ('model', utils.DBSCAN(eps=base_dbscan_eps, min_samples=base_dbscan_min_samples)),
])

base_model = ClusterFinder.ClusterFinder(base_pipe)

idx=test_id
sessionName = sessionNames_list[idx]
samples_test = df.loc[sessionName].to_numpy()

base_umap_results = base_model.special_fit_transforms(samples_test,max_depth=-1)

base_clf = base_pipe['model']
base_predicted = base_clf.fit_predict(base_umap_results)

viz.plot_umap(base_umap_results, base_predicted, title=f"base umap session {test_id}")
base_hypno_smth = viz.plot_hypnogram(base_predicted, smooth=hypnogram_smoothing, title=f"base hypnogram session {test_id}")



#%%

train_id = dat_id.copy()
train_id.remove(test_id)

samples_train = np.zeros((1,121))
for idx in train_id:
    sessionName = sessionNames_list[idx]
    samples = df.loc[sessionName].to_numpy()
    samples_train = np.concatenate((samples_train,samples),axis=0)

print(f"s1:{len(samples_train)}")
    

for i in range(len(cand_list)):
    
   
    print(f"run #{i}")

    cand_o = cand_list[i]
    
    n_rpca_components = cand_o[0]
    n_pca_components = cand_o[1]
    umap_n_neighbors = cand_o[2]
    umap_min_dist = cand_o[3]
    umap_metric = cand_o[4]
    remove_first_dim = cand_o[5]
    dbscan_eps = 0.4
    dbscan_min_samples = 20
  
    def all_but_first_column(X):
        return X[:, 1:]

    steps = []
    if (n_rpca_components>0):
        steps.append(('rpca', skRPCA.skRPCA(n_components=n_rpca_components)))
    steps.append(('pca', utils.PCA(n_components=n_pca_components)))
    if (remove_first_dim):
        steps.append(('remove_dimension', FunctionTransformer(all_but_first_column)))
    steps.append(('reduce', umap.UMAP(n_neighbors=umap_n_neighbors, min_dist=umap_min_dist, metric=umap_metric, verbose=True)))
    steps.append(('model', utils.DBSCAN(eps=dbscan_eps, min_samples=dbscan_min_samples)))
    print(cand_o)

    pipe = sklearn.pipeline.Pipeline(steps=steps)

    model = ClusterFinder.ClusterFinder(pipe)
   
    umap_results_train = model.special_fit_transforms(samples_train)    
    print("after transform")

    umap_results_test = model.just_transforms(samples_test)

    clf = pipe['model']
    predicted = clf.fit_predict(umap_results_test)
    print("after predict")

    for f in glob.glob(image_dir + '/*'):
        os.remove(f)

    mlflow.set_experiment(experiment_name)

    remove_pca0 = 'remove_dimension' in [i[0] for i in pipe.steps]

    mlflow.start_run()
    mlflow.log_params({
        'test_id': test_id,
        'n_rpca_components': n_rpca_components,
        'n_pca_components': n_pca_components,
        'umap_n_neighbors': umap_n_neighbors,
        'umap_min_dist': umap_min_dist,
        "umap_metric": umap_metric,
        "remove_pca0": remove_pca0,
        'dbscan_eps': dbscan_eps,
        'dbscan_min_samples': dbscan_min_samples,
    })

    params_log = ""
    if (n_rpca_components>0):
        params_log =params_log+f"rpca{n_rpca_components}_"
    params_log =params_log+f"pca{n_pca_components}_"
    if (remove_pca0):
        params_log =params_log+"rm0_"
    params_log =params_log+f"umap{umap_n_neighbors}-{umap_min_dist}-{umap_metric}"

    train_ids_str = "".join([str(i) for i in train_id])


    ##train 

    title = ('UMAP leave one out: training on datastes '+train_ids_str+"\n"+params_log)

    img_fname = (image_dir + "/"
                + "loo_train_"+train_ids_str+"_"+params_log+".png")

    viz.plot_umap(umap_results_train,title=title, fname=img_fname)


    #apply

    title = ('UMAP leave one out: test on session '+str(idx)+"\n"+params_log)

    img_fname = (image_dir + "/"
                + "loo_test"+str(idx)+"_"+params_log+".png")

    viz.plot_umap(umap_results_test, title=title, fname=img_fname)


    ##############
    # Hypnogram (smoothed)
    ##############


    title = ("UMAP leave one out: training on datastes "+train_ids_str+"\n"+params_log+"\n"+
              " Possible Hypnogram. SMOOTHED (window=" + str(hypnogram_smoothing) + ")\n"+
              "-labels may not be in the correct order")

    img_fname = (image_dir + "/"
                + "loo_train_"+train_ids_str+"_"+params_log
                 + "_hipnogram_smth" + str(hypnogram_smoothing) + ".png")
    
    print(np.sum(predicted),predicted.shape,predicted[:20])
    test_hypno_smth = viz.plot_hypnogram(predicted, smooth=hypnogram_smoothing, title=title, fname=img_fname)

    debug = {
            'predicted':predicted,
            'test_hypno_smth':test_hypno_smth,
            'base_hypno_smth':base_hypno_smth
            }


    try:
        score_mse = mean_squared_error(base_hypno_smth/np.max(base_hypno_smth),test_hypno_smth/np.max(test_hypno_smth))
    except:
        score_mse = -999
        with open('debug_mse.pickle', 'wb') as handle:
            pickle.dump(debug, protocol=pickle.HIGHEST_PROTOCOL)

    try:    
        score_rand = adjusted_rand_score(base_hypno_smth,test_hypno_smth)
    except:
        score_rand = -999
        with open('debug_rand.pickle', 'wb') as handle:
            pickle.dump(debug, protocol=pickle.HIGHEST_PROTOCOL)
    

    mlflow.log_metrics({'mse': score_mse})
    mlflow.log_metrics({'rand': score_rand})




    ##############
    # Cleanup
    ##############

    del pipe
    del model
    del umap_results_train
    del umap_results_test
    del clf
    del predicted
    plt.close("all")

    mlflow.log_artifacts(image_dir, "images")

    mlflow.end_run()
    

