import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from random import randint
from sklearn.metrics import silhouette_score
from sklearn.base import clone

import os
import sys
sys.path.insert(0,'lib')


from lib import DataGenerator
from lib import Vizualization
from lib import ClusterFinder
from lib import skRPCA



#%%
    
datagen = DataGenerator.DataGenerator()

pipe = Pipeline(steps=[
    #('scalar', StandardScaler()),
    #('rpca', skRPCA(n_components=2)),
    ('pca', PCA(n_components=2)),
    ('model',GaussianMixture(n_components=5,covariance_type='full'))
])

model = ClusterFinder.ClusterFinder(pipe)

vizualization = Vizualization.Vizualization()

#%%
"""
X_orig, y = datagen.generate(n_samples=2000,n_features=3, centers=4,cluster_std=0.5)
vizualization.make_chart(X_orig,y,size=(-10,10),title='Input data')
"""

#%%

def run_test(pca_n_components=2,data_n_features=2,n_centers=3,verbose=False):

    pipe['pca'].n_components=pca_n_components
    
    noise_level_2check = np.around(np.linspace(0,1.0,num=11), decimals=1)
    noise_channels_2check = np.around((np.logspace(0, 2, 10))*1.19-1, decimals=0)
    
    res=[]
    
    X_orig, y = datagen.generate(n_samples=2000,n_features=data_n_features, centers=n_centers,cluster_std=0.5)
    
    for noise_level in noise_level_2check:
        for noise_channels in noise_channels_2check:
            print("noise_level {}, noise_channels:{:1.2f}".format(noise_level,noise_channels))
            noise_level_real = noise_level+0.002*noise_channels
            X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels.astype(int), noise_level=noise_level_real)
            model.fit(X_noisy)
            model.pipe['model'].random_state = None
            optimal_k_bic = model.optimal_k_bic(X_noisy,num_iterations=10)
            optimal_k_silhouette = model.optimal_k_silhouette(X_noisy,num_iterations=10)
            optimal_k_distortions = model.optimal_k_distortions(X_noisy)
            if (verbose):
                print("bic:{},sil:{},dist:{}".format(optimal_k_bic,optimal_k_silhouette,optimal_k_distortions))
            
            res.append(np.array([noise_level_real,noise_channels,optimal_k_bic,optimal_k_silhouette,optimal_k_distortions]))


    df = pd.DataFrame(list(map(np.ravel, res)),columns=['noise_level', 'noise_channels','optimal_k_bic', 'optimal_k_silhouette','optimal_k_distortions'])
   
    fig= plt.figure(figsize=(10,10))
    
    for noise_channels in noise_channels_2check:
        group = df[df.noise_channels==noise_channels].groupby(['noise_level'])
        means = group.mean()
        stds = group.std()
        plt.errorbar(list(means.index),list(means.optimal_k_bic),list(stds.optimal_k_bic),label='noise_channels'+str(noise_channels))
    plt.ylabel("cluster detected")
    plt.xlabel("noise level")
    plt.title("{} original clusters with optimal_k_bic. how many the model predicted? (pca={},data_n_features={})".format(n_centers,pca_n_components,data_n_features))
    plt.legend(loc='upper left')
    
    fig= plt.figure(figsize=(10,10))
    
    for noise_channels in noise_channels_2check:
        group = df[df.noise_channels==noise_channels].groupby(['noise_level'])
        means = group.mean()
        stds = group.std()
        plt.errorbar(list(means.index),list(means.optimal_k_silhouette),list(stds.optimal_k_silhouette),label='noise_channels'+str(noise_channels))
    plt.ylabel("cluster detected")
    plt.xlabel("noise level")
    plt.title("{} original clusters with optimal_k_silhouette. how many the model predicted? (pca={},data_n_features={})".format(n_centers,pca_n_components,data_n_features))
    plt.legend(loc='upper left')

    fig= plt.figure(figsize=(10,10))

    for noise_channels in noise_channels_2check:
        group = df[df.noise_channels==noise_channels].groupby(['noise_level'])
        means = group.mean()
        stds = group.std()
        plt.errorbar(list(means.index),list(means.optimal_k_distortions),list(stds.optimal_k_distortions),label='noise_channels'+str(noise_channels))
    plt.ylabel("cluster detected")
    plt.xlabel("noise level")
    plt.title("{} original clusters with optimal_k_distortions. how many the model predicted? (pca={},data_n_features={})".format(n_centers,pca_n_components,data_n_features))
    plt.legend(loc='upper left')


#%%

run_test(pca_n_components=2,data_n_features=2,n_centers=5)
