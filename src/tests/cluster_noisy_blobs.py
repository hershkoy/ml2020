import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from random import randint
from sklearn.metrics import silhouette_score
from sklearn.base import clone

import os
import sys
sys.path.insert(0,'lib')


from lib import DataGenerator
from lib import Vizualization
from lib import ClusterFinder
from lib import skRPCA



#%%
    
datagen = DataGenerator.DataGenerator()

pipe = Pipeline(steps=[
    ('scalar', StandardScaler()),
    #('rpca', skRPCA(n_components=2)),
    ('pca', PCA(n_components=2)),
    ('model',GaussianMixture(n_components=5,covariance_type='full'))
])

model = ClusterFinder.ClusterFinder(pipe)

vizualization = Vizualization.Vizualization()

#%%

def test1():

    iterations = np.arange(0,5)
    noise_level_2check = np.around(np.linspace(0,0.5,num=6), decimals=1)
    noise_channels_2check = np.linspace(0,10,num=6)
    
    #index = pd.MultiIndex.from_tuples(list(zip(*[iterations,noise_level_2check,noise_channels_2check])), names=['iterations','noise_level', 'noise_channels'])
    #res = pd.Series(index=index)
    #res.dropna(inplace=True)
    
    res=[]
    
    model.pipe['model'].n_components=3
    
    for i in iterations:
        X_orig, y = datagen.generate(n_samples=500,n_features=2, minimum_distance=7,centers=3,cluster_std=0.5)
    
        for noise_level in noise_level_2check:
            for noise_channels in noise_channels_2check:
                noise_level_real = noise_level+0.002*noise_channels
                X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels.astype(int), noise_level=noise_level_real)
                rand = randint(0, 1000)
                model.pipe['model'].random_state = rand
                model.fit(X_noisy,y)
                y_pred = model.pipe.predict(X_noisy)
                score = model.score(X_noisy,y_pred)
                res.append(np.array([noise_level_real,noise_channels,score]))
            
                
    df = pd.DataFrame(list(map(np.ravel, res)),columns=['noise_level', 'noise_channels', 'score'])
    
    
    
    fig= plt.figure(figsize=(10,10))
    
    for noise_channels in noise_channels_2check:
        group = df[df.noise_channels==noise_channels].groupby(['noise_level'])
        means = group.mean()
        stds = group.std()
        plt.errorbar(list(means.index),list(means.score),list(stds.score),label='noise_channels'+str(noise_channels))
        
    plt.ylabel("sum of square of distance to cluster center")
    plt.xlabel("noise level")
    plt.title("error vs noise level/noise_channels")
    plt.legend(loc='upper left')

#%%


def test2(pca_n_components=2,data_n_features=2,n_centers=3,verbose=False):

    pipe['pca'].n_components=pca_n_components
    
    noise_level_2check = np.around(np.linspace(0,0.2,num=5), decimals=1)
    noise_channels_2check = np.linspace(0,8,num=5)
    
    res=[]
    
    X_orig, y = datagen.generate(n_samples=500,n_features=data_n_features, minimum_distance=7,centers=n_centers,cluster_std=0.5)
    
    for noise_level in noise_level_2check:
        for noise_channels in noise_channels_2check:
            print("noise_level {}, noise_channels:{:1.2f}".format(noise_level,noise_channels))
            noise_level_real = noise_level+0.002*noise_channels
            X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels.astype(int), noise_level=noise_level_real)
            model.pipe['model'].random_state = None
            optimal_k_bic = model.optimal_k_bic(X_noisy,num_iterations=10)
            optimal_k_silhouette = model.optimal_k_silhouette(X_noisy,num_iterations=10)
            optimal_k_distortions = model.optimal_k_distortions(X_noisy)
            if (verbose):
                print("bic:{},sil:{},dist:{}".format(optimal_k_bic,optimal_k_silhouette,optimal_k_distortions))
            
            res.append(np.array([noise_level_real,noise_channels,optimal_k_bic,optimal_k_silhouette,optimal_k_distortions]))


    df = pd.DataFrame(list(map(np.ravel, res)),columns=['noise_level', 'noise_channels','optimal_k_bic', 'optimal_k_silhouette','optimal_k_distortions'])
   
    fig= plt.figure(figsize=(10,10))
    
    for noise_channels in noise_channels_2check:
        group = df[df.noise_channels==noise_channels].groupby(['noise_level'])
        means = group.mean()
        stds = group.std()
        plt.errorbar(list(means.index),list(means.optimal_k_bic),list(stds.optimal_k_bic),label='noise_channels'+str(noise_channels))
    plt.ylabel("cluster detected")
    plt.xlabel("noise level")
    plt.title("{} original clusters with optimal_k_bic. how many the model predicted? (pca={},data_n_features={})".format(n_centers,pca_n_components,data_n_features))
    plt.legend(loc='upper left')
    
    fig= plt.figure(figsize=(10,10))
    
    for noise_channels in noise_channels_2check:
        group = df[df.noise_channels==noise_channels].groupby(['noise_level'])
        means = group.mean()
        stds = group.std()
        plt.errorbar(list(means.index),list(means.optimal_k_silhouette),list(stds.optimal_k_silhouette),label='noise_channels'+str(noise_channels))
    plt.ylabel("cluster detected")
    plt.xlabel("noise level")
    plt.title("{} original clusters with optimal_k_silhouette. how many the model predicted? (pca={},data_n_features={})".format(n_centers,pca_n_components,data_n_features))
    plt.legend(loc='upper left')

    fig= plt.figure(figsize=(10,10))

    for noise_channels in noise_channels_2check:
        group = df[df.noise_channels==noise_channels].groupby(['noise_level'])
        means = group.mean()
        stds = group.std()
        plt.errorbar(list(means.index),list(means.optimal_k_distortions),list(stds.optimal_k_distortions),label='noise_channels'+str(noise_channels))
    plt.ylabel("cluster detected")
    plt.xlabel("noise level")
    plt.title("{} original clusters with optimal_k_distortions. how many the model predicted? (pca={},data_n_features={})".format(n_centers,pca_n_components,data_n_features))
    plt.legend(loc='upper left')


#%%

#test1()

#%%
    
data_n_features = 2
n_centers=3
noise_channels=2
noise_level=0.05
#covariances = np.diag((0.9,0.25))
cluster_std=0.55
minimum_distance=cluster_std*5

X_orig, y = datagen.generate(
        n_samples=500,
        n_features=data_n_features, 
        #minimum_distance=minimum_distance, 
        #centers=n_centers,
        centers=[(-5,-5),(5,-5),(0,5)],
        cluster_std=cluster_std)
        #cov=covariances)

X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels, noise_level=noise_level)

           
vizualization.make_chart(X_orig,y,size=(-10,10))
#vizualization.make_chart(X_noisy,y,size=(-10,10))
#vizualization.pca_visualization(X_pca,y,title='PCA')

#print("optimal k (BIC): ",model.optimal_k_bic(X_noisy,num_iterations=10,plot=True))
#print("optimal k (silhouette): ",model.optimal_k_silhouette(X_noisy,num_iterations=10,plot=True))
#print("optimal k (distortions): ",model.optimal_k_distortions(X_noisy,plot=True))


#%%

model.pipe['model'].n_components=n_centers #assuming that we find the correct number of clusters
model.pipe['model'].covariance_type='full'
model.fit(X_noisy)

X_pca =model.just_transforms(X_noisy) 

vizualization.vizualization.pca_visualization(X_pca,y,title='PCA')


#%%

plt.figure(figsize=(10, 10))



clf = model.pipe['model']
plt.subplot(2,2,1)
vizualization.vizualization.visualize_gmm(clf,X_pca,newplot=False)
plt.title("gmm covariance_type='full'")

y_pred = model.predict(X_noisy) 
#vizualization.make_chart(X_orig,y_pred)
plt.subplot(2,2,2)
vizualization.vizualization.make_chart(X_pca,y_pred,newplot=False)
plt.title("covariance_type='full'")

model.pipe['model'].covariance_type='tied'
model.fit(X_noisy)

clf = model.pipe['model']
plt.subplot(2,2,3)
vizualization.vizualization.visualize_gmm(clf,X_pca,newplot=False)
plt.title("gmm covariance_type='tied'")


y_pred = model.predict(X_noisy) 
#vizualization.make_chart(X_orig,y_pred)
plt.subplot(2,2,4)
vizualization.vizualization.make_chart(X_pca,y_pred,newplot=False)
plt.title("covariance_type='tied'")


#%%

test2(pca_n_components=2,data_n_features=2,n_centers=3,verbose=True)


#%%
   
"""    
test2(pca_n_components=2,data_n_features=2,n_centers=3)
test2(pca_n_components=2,data_n_features=2,n_centers=4)
test2(pca_n_components=2,data_n_features=3,n_centers=3)
test2(pca_n_components=2,data_n_features=3,n_centers=4)

test2(pca_n_components=3,data_n_features=2,n_centers=3)
test2(pca_n_components=3,data_n_features=2,n_centers=4)
test2(pca_n_components=3,data_n_features=3,n_centers=3)
test2(pca_n_components=3,data_n_features=3,n_centers=4)
"""

#%%

print("until here")
import sys      
sys.exit()

#%%

    
X, y = datagen.generate(n_samples=500,centers=3, minimum_distance=7,cluster_std=0.5)
vizualization.make_chart(X,y)

X= datagen.add_noise(X,2, noise_level=0.15)

#%%

pca_dimension = 2

pca = PCA(n_components=pca_dimension)
scaler = StandardScaler()
scaler.fit(X)
X_scaled = scaler.transform(X)
pca.fit(X_scaled)
X_pca = pca.transform(X_scaled)

vizualization.pca_visualization(X_pca,y,'PCA with real y label')


#%%

gmm_type = 'tied'
gmm = GaussianMixture(n_components=3, covariance_type=gmm_type)
gmm.fit(X_pca)

vizualization.visualize_gmm(gmm,X_pca)


y_pred_GMM1 = gmm.predict(X_pca)
score = clustered_label_accuracy_score(y,y_pred_GMM1)
print("accuracy: ",score['accuracy'])

check = score['check']

vizualization.pca_visualization(X_pca,y_pred_GMM1,'PCA with predicted label')

#%%
fig = plt.figure()
scatter  = plt.scatter(X_pca[:,0], X_pca[:,1], c=y_pred_GMM1)
classes = list(set(y_pred_GMM1))
plt.legend(handles=scatter.legend_elements()[0], labels=classes)
plt.title('GMM with predicted y label')


#%%

pipe_gmm = make_pipeline(
        StandardScaler(),
        PCA(n_components=2),
        GaussianMixture(random_state=2,n_components=5,covariance_type='full'))
pipe_gmm.fit(X,y)
y_pred_GMM2 = pipe_gmm.predict(X)
#vizualization.make_chart(X, y_pred_GMM1)
print("accuracy: ",clustered_label_accuracy_score(y,y_pred_GMM2)['accuracy'])

#%%

pipe = make_pipeline(StandardScaler(),PCA(n_components=2) ,KMeans(n_clusters=10))
pipe.fit(X,y)
y_pred_KMeans1 = pipe.predict(X)
vizualization.make_chart(a, y_pred_KMeans1)
print("accuracy: ",clustered_label_accuracy_score(y,y_pred_KMeans1)['accuracy'])



#%%

print("optimal k (BIC): ",model.optimal_k_bic(X))
print("optimal k (silhouette): ",model.optimal_k_silhouette(X))

#%%

"""
model.fit(X,y)
#vizualization.make_chart(X, y_pred_GMM1)
print("final accuracy: ",model.score())

y_pred = model.pipe.predict(X)
print("accuracy: ",clustered_label_accuracy_score(y,y_pred)['accuracy'])
"""

#%%

X_pca = pipe['pca'].transform(pipe['scalar'].transform(X))
vizualization.pca_visualization(X_pca,y,'PCA with real y label')
#vizualization.pca_visualization(X_pca,y_pred,'PCA with predicted label')


#%%


def gmm_js(gmm_p, gmm_q, n_samples=10**5):
    X = gmm_p['model'].sample(n_samples)[0]
    log_p_X = gmm_p['model'].score_samples(X)
    log_q_X = gmm_q['model'].score_samples(X)
    log_mix_X = np.logaddexp(log_p_X, log_q_X)

    Y = gmm_q['model'].sample(n_samples)[0]
    log_p_Y = gmm_p['model'].score_samples(Y)
    log_q_Y = gmm_q['model'].score_samples(Y)
    log_mix_Y = np.logaddexp(log_p_Y, log_q_Y)

    return (log_p_X.mean() - (log_mix_X.mean() - np.log(2))
            + log_q_Y.mean() - (log_mix_Y.mean() - np.log(2))) / 2
            
            

#%%

gmm_js_scores = {}

pipe2=clone(pipe)
K = range(1,10)

for k in K:
    pipe.fit(X)
    pipe2.fit(X)
    gmm_js_scores[k] = gmm_js(pipe,pipe2)

#%%
    
"for testing of silhouette_score """

num_iterations=10
K = range(2,10)
silhouette = np.zeros((len(K),num_iterations))
model.pipe['model'].random_state=None

for k in K:
    for i in range(num_iterations):
        model.pipe['model'].n_components=k
        X_pca = model.transform(X)
        gmm = model.pipe['model']
        gmm.fit(X_pca) 
        y_pred1= gmm.predict(X_pca)      
        score = silhouette_score(X_pca, y_pred1)
        #print("score: ",score)          
        silhouette[k-2,i] = score
        #print("score: ",score) 

"""best five values, then average"""        
silhouette_top5=np.sort(silhouette,axis=1)[:,-5:]
plt.figure()
plt.errorbar(K, np.mean(silhouette_top5,axis=1), np.std(silhouette_top5,axis=1), marker='^')


#%%

"for testing of silhouette_score """
 
num_iterations=10
K = range(2,10)
silhouette = np.zeros((len(K),num_iterations))
model.pipe['model'].random_state=None

for k in K:
    for i in range(num_iterations):
        model.pipe['model'].n_components=k
        model.fit(X)
        y_pred2= model.predict(X)
        score = silhouette_score(X, y_pred2)            
        silhouette[k-2,i] = score
        #print("score: ",score) 

"""best five values, then average"""        
silhouette_top5=np.sort(silhouette,axis=1)[:,-5:]
plt.figure()
plt.errorbar(K, np.mean(silhouette_top5,axis=1), np.std(silhouette_top5,axis=1), marker='^')


#%%

X_orig, y = datagen.generate(n_samples=500,centers=3,cluster_std=0.5)
noise_range_2check = np.linspace(0,0.5,num=6)
noise_channels_2check = np.linspace(0,10,num=6)

fullres = {}
for noise_channels in noise_channels_2check:
    res={}
    for noise_range in noise_range_2check:
        X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels.astype(int), noise_range=(-noise_range,noise_range))
        k_to_use = model.optimal_k_bic(X)
        model.supervised_fit(X_noisy,y,num_iter=10,n_components=k_to_use)
        res[noise_range]=model.score()
    fullres[noise_channels]=res

#%%  
    
fig= plt.figure(figsize=(20,10))

for noise_channels in fullres:
    res_per_noise_channels = fullres[noise_channels]
    plt.plot(list(dict.keys(fullres[noise_channels])),list(dict.values(fullres[noise_channels])),label="noise_channels="+str(noise_channels))
plt.ylabel("accuracy")
plt.xlabel("noise level")
plt.title("accuracy per additional noise channels and noise level")
plt.legend(loc=0)
    
#%%

# test for calculating sum of square error

datagen = DataGenerator()

X_orig, y = datagen.generate(n_samples=500,n_features=2, minimum_distance=7,centers=3,cluster_std=0.5)
vizualization.make_chart(X_orig,y)
X= datagen.add_noise(X_orig, noise_channels_to_add=0, noise_level=0)

model.pipe['model'].n_components=3
model.pipe.fit(X,y)
y_pred = model.pipe.predict(X)

centers = model.inverse_transform(model.pipe['model'].means_)
centers = [ centers[i,:] for i in y_pred  ]
MSE = np.sum((X-centers)**2)/len(X)

#%%



