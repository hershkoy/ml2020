import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from random import randint
from sklearn.metrics import silhouette_score
from sklearn.base import clone

import os
import sys
sys.path.insert(0,'lib')


from lib import DataGenerator
from lib import Vizualization
from lib import ClusterFinder
from lib import skRPCA



#%%
    
datagen = DataGenerator.DataGenerator()

pipe = Pipeline(steps=[
    #('scalar', StandardScaler()),
    #('rpca', skRPCA(n_components=2)),
    ('pca', PCA(n_components=2)),
    ('model',GaussianMixture(n_components=5,covariance_type='full'))
])

model = ClusterFinder.ClusterFinder(pipe)

vizualization = Vizualization.Vizualization()

#%%

    
data_n_features = 2
n_centers=3
noise_channels=119
noise_level=0.05
covariances = np.diag((1.0,0.2))
cluster_std=0.3
minimum_distance=None

X_orig, y = datagen.generate(
        n_samples=2000,
        n_features=data_n_features, 
        minimum_distance=minimum_distance, 
        #centers=n_centers,
        centers=[[0, 5], [-1, -1], [1, -1]],
        cluster_std=cluster_std,
        cov=covariances)

X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels, noise_level=noise_level)

           
vizualization.make_chart(X_orig,y,size=(-2.2,2.2),title='Input data')
#vizualization.make_chart(X_orig,y,size=(-10,10))
#vizualization.pca_visualization(X_pca,y,title='PCA')

#print("optimal k (BIC): ",model.optimal_k_bic(X_noisy,num_iterations=10,plot=True))
#print("optimal k (silhouette): ",model.optimal_k_silhouette(X_noisy,num_iterations=10,plot=True))
#print("optimal k (distortions): ",model.optimal_k_distortions(X_noisy,plot=True))


#%%

model.pipe['model'].n_components=n_centers #assuming that we find the correct number of clusters
model.pipe['model'].covariance_type='full'
model.fit(X_noisy)

X_pca =model.just_transforms(X_noisy) 

vizualization.pca_visualization(X_pca,y,title='PCA')


#%%

plt.figure(figsize=(10, 10))



clf = model.pipe['model']
plt.subplot(2,2,1)
vizualization.visualize_gmm(clf,X_pca,newplot=False)
plt.title("gmm covariance_type='full'")

y_pred = model.predict(X_noisy) 
#vizualization.make_chart(X_orig,y_pred)
plt.subplot(2,2,2)
vizualization.make_chart(X_pca,y_pred,newplot=False)
plt.title("covariance_type='full'")

model.pipe['model'].covariance_type='tied'
model.fit(X_noisy)

clf = model.pipe['model']
plt.subplot(2,2,3)
vizualization.visualize_gmm(clf,X_pca,newplot=False)
plt.title("gmm covariance_type='tied'")


y_pred = model.predict(X_noisy) 
#vizualization.make_chart(X_orig,y_pred)
plt.subplot(2,2,4)
vizualization.make_chart(X_pca,y_pred,newplot=False)
plt.title("covariance_type='tied'")
