from tensorflow.keras.layers import Input ,Dense, Dropout, Activation, LSTM,Masking
from tensorflow.keras.layers import Lambda, Conv2D, MaxPooling2D, Flatten, Reshape
from tensorflow.keras.layers import Conv1D, MaxPooling1D
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers.wrappers import TimeDistributed
from tensorflow.keras.layers.pooling import GlobalAveragePooling1D
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.utils import np_utils
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K


import numpy as np


nb_epoch=5;

data= np.random.random((6,2105,15,121))  #6 is the number of sessions. 2105 windows of 15 seconds in each session. 
label=np.random.random((6,2105,1))

X_train=data
y_train=label

#%%

model=Sequential();   

model.add(Masking(mask_value=-2., input_shape=X_train.shape[2:]))

model.add(TimeDistributed(Conv1D(16,8, padding='same'))) 
model.add(TimeDistributed(Activation('relu')))
#model.add(TimeDistributed(MaxPooling2D(pool_size=(3,3))))
model.add(TimeDistributed(Conv1D(32,8)))
model.add(TimeDistributed(Activation('relu')))
model.add(TimeDistributed(Conv1D(64,8)))
model.add(TimeDistributed(Activation('relu')))

model.add(TimeDistributed(MaxPooling1D(pool_size=(8))))
model.add(TimeDistributed(Dropout(0.25)))

model.add(TimeDistributed(Flatten()))
model.add(TimeDistributed(Dense(64, name="first_dense" )))

model.add(LSTM(units=40, return_sequences=True))

#model.add(Flatten())
model.add(Dense(6, activation='softmax', input_shape=(None,40)))

print(model.summary())

#%%

"""

model for input 840x30x121

model.add(TimeDistributed(Conv2D(16,3,3, padding='same'), input_shape=X_train.shape[1:])) 
model.add(TimeDistributed(Activation('relu')))
model.add(TimeDistributed(Conv2D(32,3,3)))
model.add(TimeDistributed(Activation('relu')))
model.add(TimeDistributed(Conv2D(64,3,3)))
model.add(TimeDistributed(Flatten()))
model.add(TimeDistributed(Dense(64, name="first_dense" )))

model.add(LSTM(units=20, return_sequences=True))
#output dimension here is ??

#time_distributed_merge_layer = Lambda(function=lambda x: K.mean(x, axis=1, keepdims=False))
#model.add(time_distributed_merge_layer)

#model.add(Flatten())
model.add(Dense(6, activation='softmax', input_shape=(None,20)))

print(model.summary())
"""

#%%


model.compile(loss='sparse_categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

model.fit(X_train, y_train,
          epochs=nb_epoch,
          verbose=1)


#%%

timesteps=100;
number_of_samples=2500;
nb_samples=number_of_samples;
frame_row=32;
frame_col=32;
channels=3;

nb_epoch=1;
batch_size=timesteps;

data= np.random.random((2500,timesteps,frame_row,frame_col,channels))
label=np.random.random((2500,timesteps,1))

X_train=data[0:2000,:]
y_train=label[0:2000]

X_test=data[2000:,:]
y_test=label[2000:,:]

#%%

model=Sequential();                          

model.add(TimeDistributed(Conv2D(32, 3, 3, padding='same'), input_shape=X_train.shape[1:]))
model.add(TimeDistributed(Activation('relu')))
model.add(TimeDistributed(Conv2D(32, 3, 3)))
model.add(TimeDistributed(Activation('relu')))
model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2))))
model.add(TimeDistributed(Dropout(0.25)))

model.add(TimeDistributed(Flatten()))
model.add(TimeDistributed(Dense(512)))
#output dimension here is (None, 100, 512)                

model.add(TimeDistributed(Dense(35, name="first_dense" )))
#output dimension here is (None, 100, 35)                


model.add(LSTM(units=40, return_sequences=True))
#output dimension here is (None, 100, 20)

time_distributed_merge_layer = Lambda(function=lambda x: K.mean(x, axis=1, keepdims=False))

model.add(time_distributed_merge_layer)
#output dimension here is (None, 1, 20)


#model.add(Flatten())
model.add(Dense(6, activation='sigmoid', input_shape=(None,40)))

print(model.summary())
