import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.layers import LSTM,Bidirectional
import numpy as np

window_size = 15
n_classes = 6
data_size = 32

x_data = np.random.normal(0, 1, (data_size, window_size, 122))
y_data = np.random.randint(0, n_classes, (data_size, 1))

import tensorflow as tf

tf.compat.v1.keras.backend.clear_session()

inputs = tf.keras.layers.Input((window_size, 122))

conv1 =  tf.keras.layers.Conv1D(16, kernel_size=8, padding='SAME')
res = conv1(inputs)
res = Bidirectional(LSTM(32,return_sequences=True))(res)
res = Bidirectional(LSTM(32))(res)
res = tf.keras.layers.Dense(32, activation=tf.nn.leaky_relu)(res)
res = tf.keras.layers.Dense(n_classes, activation=tf.nn.softmax)(res)

model = tf.keras.models.Model(inputs, res)
model.compile(optimizer='rmsprop', loss='sparse_categorical_crossentropy')
model.fit(x_data, y_data, batch_size=4, epochs=5)