import tensorflow as tf


import tensorflow.keras.backend as K


import numpy as np
n_classes = 6
data_size = 6*32  # 6 sessions, each has 32 samples
label_size = data_size   # extrapolated 

x_data = np.random.normal(0, 1, (data_size, 122))
y_data = np.random.randint(0, n_classes, (label_size, 1))


tf.compat.v1.keras.backend.clear_session()

inputs = tf.keras.layers.Input((data_size, 122))

res =  tf.keras.layers.Conv1D(16, kernel_size=8, padding='SAME')(inputs)
res = tf.keras.layers.LSTM(32)(res)
res = tf.keras.layers.Dense(32, activation=tf.nn.leaky_relu)(res)
res = tf.keras.layers.Dense(n_classes, activation=tf.nn.softmax)(res)

model = tf.keras.models.Model(inputs, res)
model.compile(optimizer='rmsprop', loss='sparse_categorical_crossentropy')
model.fit(x_data, y_data, batch_size=4, epochs=5)