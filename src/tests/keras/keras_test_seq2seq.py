import tensorflow as tf
import tensorflow.keras.backend as K

import numpy as np


def flatten(name, input_var):
    dim = 1
    for d in input_var.get_shape()[1:].as_list():
        dim *= d
    output_var = tf.reshape(input_var,
                            shape=[-1, dim],
                            name=name)

data_size = 3000
keep_prob_ = 0.8

batch_size=20
num_units=128
embed_size=10
input_depth=3000
n_channels=100
use_attention=True
lstm_layers=1
attention_size=64
beam_width=4
use_beamsearch_decode=False
max_time_step=10  # 5 3 second best 10# 40 # 100
output_max_length=10 + 2  # max_time_step +1



tf.compat.v1.keras.backend.clear_session()



input_var = tf.keras.layers.Input((data_size, 1))

print(input_var)


# Convolution
network1 = tf.keras.layers.Conv1D(filters=64, kernel_size=50, strides=6,
                         padding='same', activation=tf.nn.relu)(input_var)

network1 = tf.keras.layers.MaxPooling1D( pool_size=8, strides=8, padding='same')(network1)

# Dropout
network1 = tf.keras.layers.Dropout(keep_prob_)(network1)


# Convolution
network1 = tf.keras.layers.Conv1D( filters=128, kernel_size=8, strides=1,
                         padding='same', activation=tf.nn.relu)(network1)

network1 = tf.keras.layers.Conv1D( filters=128, kernel_size=8, strides=1,
                         padding='same', activation=tf.nn.relu)(network1)
network1 = tf.keras.layers.Conv1D( filters=128, kernel_size=8, strides=1,
                         padding='same', activation=tf.nn.relu)(network1)


# Max pooling
network1 = tf.keras.layers.MaxPooling1D( pool_size=4, strides=4, padding='same')(network1)


print(f"network1:{network1}")

# Flatten
network1 = tf.keras.layers.Flatten(name="flat1")(network1)

print(f"network1_flat:{network1}")


#output_conns.append(network)

######### CNNs with large filter size at the first layer #########



# Convolution
network2 = tf.keras.layers.Conv1D( filters=64, kernel_size=400, strides=50,
                           padding='same', activation=tf.nn.relu)(input_var)

network2 = tf.keras.layers.MaxPooling1D( pool_size=4, strides=4, padding='same')(network2)

# Dropout
network2 = tf.keras.layers.Dropout(keep_prob_)(network2)

# Convolution
network2 = tf.keras.layers.Conv1D( filters=128, kernel_size=6, strides=1,
                           padding='same', activation=tf.nn.relu)(network2)

network2 = tf.keras.layers.Conv1D( filters=128, kernel_size=6, strides=1,
                           padding='same', activation=tf.nn.relu)(network2)
network2 = tf.keras.layers.Conv1D( filters=128, kernel_size=6, strides=1,
                           padding='same', activation=tf.nn.relu)(network2)

# Max pooling
network2 = tf.keras.layers.MaxPooling1D( pool_size=2, strides=2, padding='same')(network2)


print(f"network2:{network2}")

# Flatten
network2 = tf.keras.layers.Flatten(name="flat2")(network2)

print(f"network2_flat:{network2}")


# Concat

concated = tf.keras.layers.concatenate(
    [network1, network2], axis=-1)

# Dropout
network = tf.keras.layers.Dropout(keep_prob_)(concated)

shape = network.get_shape().as_list()

print(f"pre encoder:{shape}")


network = tf.reshape(network, (-1, max_time_step, shape[1]))

print(f"pre encoder reshaped:{network}")


#network = Bidirectional(LSTM(num_units,return_sequences=True))(network)

encoder_lstm = Bidirectional(LSTM(num_units, return_sequences=True, return_state=True))

print(network)
network, forward_h, forward_c, backward_h, backward_c = encoder_lstm(network)

# We discard `encoder_outputs` and only keep the states.
state_h = tf.keras.layers.Concatenate()([forward_h, backward_h])
state_c = tf.keras.layers.Concatenate()([forward_c, backward_c])

print(f"state_h:{state_h}")
print(f"state_c:{state_c}")


encoder_states = [state_h, state_c]

# Set up the decoder, using `encoder_states` as initial state.
decoder_inputs = tf.keras.layers.Input(shape=(None, state_h.shape[0]))

# We set up our decoder to return full output sequences,
# and to return internal states as well. We don't use the
# return states in the training model, but we will use them in inference.
decoder_lstm = Bidirectional(LSTM(num_units, return_sequences=True, return_state=True))
decoder_outputs, _, _ = decoder_lstm(decoder_inputs,
                                     initial_state=encoder_states)


out = Dense(num_decoder_tokens, activation='softmax')(decoder_outputs)

