import tensorflow as tf


import tensorflow.keras.backend as K


import numpy as np
window_size = 15
n_classes = 6
n_clusters = 30
data_size = 32

x_data = np.random.normal(0, 1, (data_size, window_size, 122))
cluster_data = np.random.randint(0, n_clusters, (data_size,window_size ))
y_data = np.random.randint(0, n_classes, (data_size, 1))

tf.compat.v1.keras.backend.clear_session()

sample_inputs = tf.keras.layers.Input((window_size, 122))
cluster_inputs = tf.keras.layers.Input((15,), dtype=tf.int32)

embedding_layer = tf.keras.layers.Embedding(input_dim=n_clusters + 1,
                                            output_dim=25,
                                            input_length=15)
embedded_clusters = embedding_layer(cluster_inputs)
#embedded_clusters = tf.keras.layers.Lambda(lambda x: tf.squeeze(x, 1))(embedded_clusters)

concated = tf.keras.layers.concatenate(
    [sample_inputs, embedded_clusters], axis=-1)


res = tf.keras.layers.LSTM(32,dropout=0.0)(concated)
res = tf.keras.layers.Dense(32, activation=tf.nn.leaky_relu)(res)
res = tf.keras.layers.Dense(n_classes, activation=tf.nn.softmax)(res)
model = tf.keras.models.Model([sample_inputs, cluster_inputs], res)


model.compile(optimizer='rmsprop',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit([x_data, cluster_data],
          y_data,
          batch_size=4,
          #callbacks=[LossAndErrorPrintingCallback()],
          #callbacks=[WandbCallback()],
          epochs=5)