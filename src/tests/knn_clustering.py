import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from random import randint
from sklearn.metrics import silhouette_score
from sklearn.base import clone
from sklearn.neighbors import NearestNeighbors
from scipy.spatial.distance import cdist
import scipy.cluster.hierarchy as sch
from  sklearn.cluster import AgglomerativeClustering
import os
import sys
#sys.path.insert(0,'lib')
sys.path.insert(0,os.getcwd()+'\\code\\lib')


from lib import DataGenerator
from lib import Vizualization
from lib import ClusterFinder
from lib import skRPCA



#%%
    
datagen = DataGenerator.DataGenerator()

vizualization = Vizualization.Vizualization()

#%%

    
data_n_features = 2
n_centers=2
noise_channels=0
noise_level=0
covariances = None
cluster_std=0.3
minimum_distance=None

X_orig, y = datagen.generate(
        n_samples=500,
        n_features=data_n_features, 
        minimum_distance=minimum_distance, 
        #centers=n_centers,
        centers=[[-3, -3], [3, 3]],
        cluster_std=cluster_std,
        cov=covariances)

X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels, noise_level=noise_level)

           
vizualization.make_chart(X_noisy,y,size=(-6,6),title='Input data')
#vizualization.make_chart(X_orig,y,size=(-10,10))
#vizualization.pca_visualization(X_pca,y,title='PCA')

#print("optimal k (BIC): ",model.optimal_k_bic(X_noisy,num_iterations=10,plot=True))
#print("optimal k (silhouette): ",model.optimal_k_silhouette(X_noisy,num_iterations=10,plot=True))
#print("optimal k (distortions): ",model.optimal_k_distortions(X_noisy,plot=True))


#%%

nbrs = NearestNeighbors(n_neighbors=3, algorithm='ball_tree').fit(X_noisy)
distances, indices = nbrs.kneighbors(X_noisy)

#%%

#todo: 
# change to increase all points at once
# automatic threshold set

threshold = 0.2
min_cluster_ratio = 0.07 # percent of number of points


all_points = set(np.arange(X_noisy.shape[0]))
remaining_points = all_points
clustered_points = set()
clusters = []
outliers = []
cluster = set([0])
points2chk = set([0])
idx2cluster = (np.ones(X_noisy.shape[0])*-1).astype('int32')


min_cluster_size = round(X_noisy.shape[0]*min_cluster_ratio)

dist = cdist(X_noisy,X_noisy,'euclidean')

while True:
    #print("cluster:",len(cluster))
    pos = points2chk.pop()
    new_points = indices[pos,:]
    new_points = set(new_points)-clustered_points
    points2chk.update(new_points-cluster)
    cluster.update(new_points)
    
    if len(points2chk)==0:
        #try to add more points to current cluster that are within threshold distance from cluster points
        candidates = dist[list(cluster),:]
        candidates = set(np.transpose((candidates<threshold).nonzero())[:,1])
        points2chk.update(candidates-cluster-clustered_points)
    
    if len(points2chk)==0:
        #try to start a new cluster
        print("remaining_points:",len(remaining_points),"cluster:",len(cluster))
        idx2cluster[list(cluster)]=len(clusters)
        clusters.append(cluster)
        clustered_points = clustered_points.union(cluster)
        remaining_points= remaining_points-cluster
        if len(remaining_points)==0:
            break
        
        pos = np.argmin(distances[list(remaining_points),1])
        pos = list(remaining_points)[pos]
        cluster = set([pos])
        points2chk = set([pos])
 
#%%        

#agglomerate small clusters
num_points_in_cluster = [len(c) for c in clusters]

distance_to_near_cluster=[]
closest_cluster = (np.array([])).astype('int32')
avg_within_cluster_distance = []
for i in range(len(clusters)):
    c= clusters[i]
    avg_within_cluster_distance.append(np.mean(distances[list(c),1:]))
    non_cluster_points = list(all_points-c)
    non_cluster_distance_matrix = dist[np.ix_(list(c),non_cluster_points)]
    distance_to_near_cluster.append(np.min(non_cluster_distance_matrix))
    pos = np.argmin(np.min(non_cluster_distance_matrix,axis=0))
    pos = non_cluster_points[pos]
    closest_cluster = np.append(closest_cluster,idx2cluster[pos])

#%%

for i in range(len(clusters)):
    if num_points_in_cluster[i]>min_cluster_size:
        continue
    clusters[closest_cluster[i]].update(clusters[i])
    closest_cluster[closest_cluster==i] = closest_cluster[i]
    clusters[i] = set()

clusters = [c for c in clusters if len(c)>0]
#%%

vizualization.make_chart(X_noisy,y,size=(-6,6),title='results (final)')
plt.scatter(X_noisy[list(clusters[0]),0],X_noisy[list(clusters[0]),1])
plt.scatter(X_noisy[list(clusters[1]),0],X_noisy[list(clusters[1]),1])
