import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from random import randint
from sklearn.metrics import silhouette_score
from sklearn.base import clone
from sklearn.neighbors import NearestNeighbors
from scipy.spatial.distance import cdist
import scipy.cluster.hierarchy as sch
from  sklearn.cluster import AgglomerativeClustering
import os
import sys
sys.path.insert(0,'lib')
sys.path.insert(0,os.getcwd()+'\\code\\lib')


from lib import DataGenerator
from lib import Vizualization
from lib import ClusterFinderKNN
from lib import skRPCA



#%%
    
datagen = DataGenerator.DataGenerator(method='moons') 

pipe = Pipeline(steps=[
    ('pca', PCA(n_components=2)),
    ('model',NearestNeighbors(n_neighbors=3, algorithm='ball_tree'))
])

model = ClusterFinderKNN.ClusterFinderKNN(pipe)


vizualization = Vizualization.Vizualization()

#%%

    
data_n_features = 2
n_centers=2
noise_channels=20
noise_level=0.1
covariances = None
cluster_std=0.3
minimum_distance=None

X_orig, y = datagen.generate(
        n_samples=500,
        n_features=data_n_features, 
        minimum_distance=minimum_distance, 
        #centers=n_centers,
        centers=[[-3, -3], [3, 3]],
        cluster_std=cluster_std,        
        cov=covariances)

X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels, noise_level=noise_level)

           
vizualization.make_chart(X_noisy,y,size=(-6,6),title='Input data')
#vizualization.make_chart(X_orig,y,size=(-10,10))
X_pca = PCA(2).fit_transform(X_noisy)
vizualization.pca_visualization(X_pca,y,title='PCA')

#print("optimal k (BIC): ",model.optimal_k_bic(X_noisy,num_iterations=10,plot=True))
#print("optimal k (silhouette): ",model.optimal_k_silhouette(X_noisy,num_iterations=10,plot=True))
#print("optimal k (distortions): ",model.optimal_k_distortions(X_noisy,plot=True))


#%%

model.fit(X_noisy)
clusters = model.get_clusters()
k =  model.get_k()

#%%

vizualization.make_chart(X_noisy[:,:2],y,size=(-6,6),title='Final')
plt.scatter(X_noisy[list(clusters[0]),0],X_noisy[list(clusters[0]),1])
if (len(clusters)>1):
    plt.scatter(X_noisy[list(clusters[1]),0],X_noisy[list(clusters[1]),1])

print("k:",k)
