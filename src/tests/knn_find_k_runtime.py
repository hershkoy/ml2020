import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from random import randint
from sklearn.metrics import silhouette_score
from sklearn.base import clone
from sklearn.neighbors import NearestNeighbors
from scipy.spatial.distance import cdist
import scipy.cluster.hierarchy as sch
from  sklearn.cluster import AgglomerativeClustering
import os
import sys
sys.path.insert(0,os.getcwd()+'\\code\\lib')
import timeit

from lib import DataGenerator
from lib import Vizualization
from lib import ClusterFinderKNN
from lib import skRPCA



#%%
    
datagen = DataGenerator.DataGenerator()

pipe = Pipeline(steps=[
    ('pca', PCA(n_components=2)),
    ('model',NearestNeighbors(n_neighbors=3, algorithm='ball_tree'))
])

model = ClusterFinderKNN.ClusterFinderKNN(pipe)

vizualization = Vizualization.Vizualization()

#%%
"""
X_orig, y = datagen.generate(n_samples=2000,n_features=3, centers=4,cluster_std=0.5)
vizualization.make_chart(X_orig,y,size=(-10,10),title='Input data')
"""

#%%

def run_test(pca_n_components=2,data_n_features=2,n_centers=3,verbose=False):

    pipe['pca'].n_components=pca_n_components
    
    noise_level = 0.1
    noise_channels = 25
    
    samples = np.around((np.logspace(2.75, 4.4, 7)), decimals=0)
    
    res=[]
    
    for smp in samples:
        
            X_orig, y = datagen.generate(n_samples=smp.astype(int),n_features=data_n_features, centers=n_centers,cluster_std=0.5)
           
            start_time = timeit.default_timer()
            
            print("noise_level {}, noise_channels:{:1.2f}".format(noise_level,noise_channels))
            X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels, noise_level=noise_level)
            model.fit(X_noisy)
            k = model.get_k()
            if (verbose):
                print("k:",k)
                
            elapsed = timeit.default_timer() - start_time
            
            res.append(np.array([smp,elapsed]))

    df = pd.DataFrame(list(map(np.ravel, res)),columns=['sample','elapsed'])
   
    fig= plt.figure(figsize=(10,10))

    plt.plot(df["sample"].tolist(),df["elapsed"].tolist())
    plt.ylabel("runtime")
    plt.xlabel("samples")
    plt.title("KNN clustering Runtime")
    



#%%

run_test(pca_n_components=2,data_n_features=2,n_centers=3)
