import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from random import randint
from sklearn.metrics import silhouette_score
from sklearn.base import clone
from sklearn.neighbors import NearestNeighbors
from scipy.spatial.distance import cdist
import scipy.cluster.hierarchy as sch
from  sklearn.cluster import AgglomerativeClustering
import os
import sys
sys.path.insert(0,'lib')
sys.path.insert(0,os.getcwd()+'\\code\\lib')


from lib import DataGenerator
from lib import Vizualization
from lib import ClusterFinderKNN
from lib import skRPCA



#%%
    
datagen = DataGenerator.DataGenerator()

pipe = Pipeline(steps=[
    ('pca', PCA(n_components=2)),
    ('model',NearestNeighbors(n_neighbors=3, algorithm='ball_tree'))
])

model = ClusterFinderKNN.ClusterFinderKNN(pipe)

vizualization = Vizualization.Vizualization()

#%%
"""
X_orig, y = datagen.generate(n_samples=2000,n_features=3, centers=4,cluster_std=0.5)
vizualization.make_chart(X_orig,y,size=(-10,10),title='Input data')
"""

#%%

def run_test(pca_n_components=2,n_neighbors=3,data_n_features=2,n_centers=3,verbose=False):

    pipe['pca'].n_components=pca_n_components
    pipe['model'].n_neighbors=n_neighbors
    
    noise_level_2check = np.around(np.linspace(0,0.45,num=15), decimals=2)
    noise_channels_2check = np.linspace(0,8,num=5)
    
    res=[]
    
    X_orig, y = datagen.generate(n_samples=2000,n_features=data_n_features, centers=n_centers,cluster_std=0.5)
    
    for noise_level in noise_level_2check:
        for noise_channels in noise_channels_2check:
            print("noise_level {}, noise_channels:{:1.2f}".format(noise_level,noise_channels))
            noise_level_real = noise_level+0.002*noise_channels
            X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels.astype(int), noise_level=noise_level_real)
            model.fit(X_noisy)
            k = model.get_k()
            if (verbose):
                print("k:",k)
            
            res.append(np.array([noise_level_real,noise_channels,k]))

    df = pd.DataFrame(list(map(np.ravel, res)),columns=['noise_level', 'noise_channels','k'])
   
    fig= plt.figure(figsize=(10,10))
    
    for noise_channels in noise_channels_2check:
        group = df[df.noise_channels==noise_channels].groupby(['noise_level'])
        means = group.mean()
        stds = group.std()
        plt.errorbar(list(means.index),list(means.k),list(stds.k),label='noise_channels'+str(noise_channels))
    plt.ylabel("cluster detected")
    plt.xlabel("noise level")
    plt.title("KNN clustering. {} original clusters. how many the model predicted? (pca={},data_n_features={},KNN_neighbors={})".format(n_centers,pca_n_components,data_n_features,n_neighbors))
    plt.legend(loc='upper left')
    



#%%

run_test(n_neighbors=3,pca_n_components=2,data_n_features=2,n_centers=3)
run_test(n_neighbors=5,pca_n_components=2,data_n_features=2,n_centers=3)
