import pickle
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from scipy import stats
from sklearn.decomposition import PCA
import umap
import numba.targets


df = pd.read_pickle('df_features_test_env_unlabeled.pkl')
ind = df.index
sessionNames_list = df.index.levels[0]

reducer = umap.UMAP(n_neighbors=30,min_dist=0.0)
pca = PCA(n_components=10)

def plot_hypnogram(labels,timestamp):
    if labels[i]==-1:
        labels[i] = labels[i-1]
    smoothed = np.zeros((len(labels),1))
    for i in np.arange(15,len(labels)-15):
        smoothed[i] = stats.mode(labels[i-15:i+14])[0]
    plt.plot(timestamp,smoothed)
    plt.xlabel('Time')
    plt.ylabel('Class')
    plt.title('Hypnogram')


dat_id = [0,1,2,3,4,5,6]
c=1
plt.figure(figsize=(20,20))
for d in np.arange(len(dat_id)):
    test_id = dat_id[0]
    train_id = dat_id[1:]
    samples_train = np.zeros((1,121))
    for i in train_id:
        idx=i
        sessionName = sessionNames_list[idx]
        samples = df.loc[sessionName].to_numpy()
        samples_train = np.concatenate((samples_train,samples),axis=0)
    idx=test_id
    sessionName = sessionNames_list[idx]
    samples_test = df.loc[sessionName].to_numpy()
    
    pca_samples_train = pca.fit_transform(samples_train)
    pca_samples_test = pca.transform(samples_test)
    embedding_train = reducer.fit_transform(pca_samples_train[:,1:])
    embedding_test_by_train = reducer.transform(pca_samples_test[:,1:])
    embedding_test = reducer.fit_transform(pca_samples_test[:,1:])
    
    plt.subplot(7,2,c)
    plt.scatter(embedding_train[:, 0], embedding_train[:, 1],s=1)
    plt.gca().set_aspect('equal', 'datalim')
    plt.title('UMAP training on datastes '+str(train_id), fontsize=12);
    plt.subplot(7,2,c+1)
    plt.scatter(embedding_test_by_train[:, 0], embedding_test_by_train[:, 1],s=1)
    plt.gca().set_aspect('equal', 'datalim')
    plt.title('UMAP projection on dataset'+str(test_id), fontsize=12);
    c+=2
    dat_id = np.concatenate((train_id,np.expand_dims(test_id,-1)),axis=0)