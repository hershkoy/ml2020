"""
This file check effectiveness of UMAP in presence of noise
based on simulatd data 

"""


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import FunctionTransformer
import sklearn

import os
import sys
sys.path.insert(0,os.path.join(os.getcwd(),"lib"))


import DataGenerator
import Vizualization
import ClusterFinder
import skRPCA
import experiment_utils as utils

from Vizualization import FFormatter


#%%
    
datagen = DataGenerator.DataGenerator()

n_rpca_components = 0
n_pca_components = 0
umap_n_neighbors = 30
umap_min_dist = 0.1
umap_metric = 'euclidean'
dbscan_eps = 0.4
dbscan_min_samples = 20
remove_first_dim=False


steps = []
steps2dbg = []

if (n_rpca_components>0):
    steps.append(('rpca', skRPCA.skRPCA(n_components=n_rpca_components)))
    steps2dbg.append(f"rpca{n_rpca_components}")

if (n_pca_components>0):        
    steps.append(('pca', utils.PCA(n_components=n_pca_components)))
    steps2dbg.append(f"rpca{n_pca_components}")

if (remove_first_dim):
    steps.append(('remove_dimension', FunctionTransformer(utils.all_but_first_column)))
    steps2dbg.append(f"rmv1d")

steps.append(('reduce', utils.umap.UMAP(n_neighbors=umap_n_neighbors, min_dist=umap_min_dist, metric=umap_metric,verbose=True)))
steps2dbg.append(f"umap({umap_n_neighbors},{umap_min_dist})")

steps.append(('model', utils.DBSCAN(eps=dbscan_eps, min_samples=dbscan_min_samples)))
steps2dbg.append(f"dbscan({dbscan_eps},{dbscan_min_samples})")

pipe = sklearn.pipeline.Pipeline(steps=steps)

model = ClusterFinder.ClusterFinder(pipe)

viz = Vizualization.Vizualization()


#%%


data_n_features = 2
n_centers=3
noise_channels=119
noise_level=0.40
covariances = np.diag((1.0,0.2))
cluster_std=0.2
minimum_distance=None

X_orig, y = datagen.generate(
        n_samples=2000,
        n_features=data_n_features, 
        minimum_distance=minimum_distance, 
        #centers=n_centers,
        #cov=covariances
        centers=[[0, 1], [-1, -1], [1, -1]],
        cluster_std=cluster_std
        )

X_noisy= datagen.add_noise(X_orig, noise_channels_to_add=noise_channels, noise_level=noise_level)

viz.make_chart(X_orig,y,size=(-2.2,2.2),title='Input data')

#%%

umap_results = model.special_fit_transforms(X_noisy)
clf = pipe['model']
predicted = clf.fit_predict(umap_results)

score_rand = utils.adjusted_rand_score(y,predicted)


#%%
fig = plt.figure(figsize=(6, 12))

ax1= fig.add_subplot(3,1,1)

title = ("Input data \n"+
          f"rand score={score_rand}\n"+
          f"features={data_n_features},centers={n_centers}\n"+
          f"noise_channels={noise_channels},noise_level={noise_level}")
viz.make_chart(X_orig,y,size=(-2.2,2.2),title=title,ax=ax1,newplot=False)

ax2= fig.add_subplot(3,1,2)

model_str = ' '.join(steps2dbg)
title = (f"model: {model_str}\n"+
         "true labels")
viz.plot_umap(umap_results,pred=y,title=title,ax=ax2)

ax3= fig.add_subplot(3,1,3)
viz.plot_umap(umap_results,pred=predicted,title="model - predicted",ax=ax3)

fig.tight_layout()
plt.show()


#%%

"""
Looking for cutoff point
"""

def test_cutoff(config,noise_level):
    X_orig, y = datagen.generate(
        n_samples=config['n_samples'],
        n_features=config['data_n_features'], 
        minimum_distance=config['minimum_distance'], 
        #centers=n_centers,
        #cov=covariances
        centers=config['centers'],
        cluster_std=config['cluster_std']
        )
   
    X_noisy= datagen.add_noise(
        X_orig, 
        noise_channels_to_add=config['noise_channels'],
        noise_level=noise_level)
    umap_results = model.special_fit_transforms(X_noisy)
    clf = pipe['model']
    predicted = clf.fit_predict(umap_results)
    score_rand = utils.adjusted_rand_score(y,predicted)
    return score_rand


#%%

num_iterations=3

config = {
    'data_n_features': 2,
    'noise_channels':119,
    'centers': [[0, 1], [-1, -1], [1, -1]],
    'covariances': None, #np.diag((1.0,0.2)),
    'minimum_distance': None
}

config['n_samples']=4000
config['cluster_std']=0.1

noise_level_2test = np.arange(0.35,0.5,0.01)

res1 = []
for noise_level in noise_level_2test:
    res_t = []
    for _ in range(num_iterations):
        score = test_cutoff(config,noise_level)
        res_t.append(score)
    res1.append(res_t)


title = (f"UMAP ajusted rand index on simulated data \n"+
         f"num iterations={num_iterations}\n"+
         "data: centers="+str(len(config['centers']))+
         " std="+str(config['cluster_std'])+" n_samples="+str(config['n_samples'])+"\n"+
         "noise: levels="+str(config['noise_channels']))

noise_level_2test_ticks_str = np.round(noise_level_2test,decimals=2).astype(str)

fig = plt.figure(figsize=(8,6))
plt.title(title)
plt.boxplot(res1,widths = 0.005,positions=noise_level_2test,labels=noise_level_2test_ticks_str)
plt.xlim(0.34,0.51)
plt.show()
        

#%%

config['n_samples']=2000
config['cluster_std']=0.2

noise_level_2test = np.arange(0.28,0.45,0.01)

res2 = []
for noise_level in noise_level_2test:
    res_t = []
    for _ in range(num_iterations):
        score = test_cutoff(config,noise_level)
        res_t.append(score)
    res2.append(res_t)

title = (f"UMAP ajusted rand index on simulated data \n"+
         f"num iterations={num_iterations}\n"+
         "data: centers="+str(len(config['centers']))+
         " std="+str(config['cluster_std'])+" n_samples="+str(config['n_samples'])+"\n"+
         "noise: levels="+str(config['noise_channels']))

noise_level_2test_ticks_str = np.round(noise_level_2test,decimals=2).astype(str)

fig = plt.figure(figsize=(8,6))
plt.title(title)
plt.boxplot(res2,widths = 0.005,positions=noise_level_2test,labels=noise_level_2test_ticks_str)
plt.xlim(0.27,0.46)
plt.show()



#%%
 
# fake up some data
spread = np.random.rand(50) * 100
center = np.ones(25) * 50
flier_high = np.random.rand(10) * 100 + 100
flier_low = np.random.rand(10) * -100
data = np.concatenate((spread, center, flier_high, flier_low))
       
spread = np.random.rand(50) * 100
center = np.ones(25) * 40
flier_high = np.random.rand(10) * 100 + 100
flier_low = np.random.rand(10) * -100
d2 = np.concatenate((spread, center, flier_high, flier_low))
data.shape = (-1, 1)
d2.shape = (-1, 1)

data = [data, d2, d2[::2,0]]
fig7, ax7 = plt.subplots()
ax7.set_title('Multiple Samples with Different sizes')
ax7.boxplot(data)

plt.show()
