# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import numpy as np
from sklearn.cluster import DBSCAN


# %%
# load sleep data from pkl file
infile = open('../data/df_features_test_env_unlabeled.pkl','rb')
obj = pickle.load(infile)
infile.close()


# %%
obj.head()
#obj.to_csv('sleep.csv')


# %%
ind = obj.index
ind.names


# %%
# get recording times
recs = ind.get_level_values(0)


# %%
sessionNames_list = obj.index.levels[0]
print(sessionNames_list)


# %%
# get samples from sessionName 'idx'
idx=6
sessionName = sessionNames_list[idx]
samples = obj.loc[sessionName].get_values()
timestamp = obj.index.levels[2][:len(samples)]

print(sessionName,samples.shape, timestamp.shape)



# %%
# first PCA to 50 then t-SNE to 2 dimensions

# Sanity check for PCA - can reduce dimensionality if t-SNE takes too long
pca_components = 50
pca = PCA(n_components=pca_components)
pca_result = pca.fit_transform(samples)
print('Cumulative explained variation for 2 principal components: {}'.format(np.sum(pca.explained_variance_ratio_)))

tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
tsne_results = tsne.fit_transform(pca_result)


# %%
# cluster based on DBSCAN (using KNN for distances between samples)
clf = DBSCAN(eps=0.4,min_samples=20)
c = clf.fit_predict(tsne_results)


# plot 2-D decomposition with clustering result
df = pd.DataFrame()
df['tsne-2d-one'] = tsne_results[:,0]
df['tsne-2d-two'] = tsne_results[:,1]
plt.figure(figsize=(16,10))
sns.scatterplot(
    x="tsne-2d-one", y="tsne-2d-two",
    hue = c,
    palette=sns.color_palette("hls", len(np.unique(c))),
    data=df,
    legend="full",
    alpha=0.3
)
plt.title("Session ID-"+str(idx)+" "+sessionName+" TSNE+PCA="+str(pca_components)+" \n explained variation:"+str(np.sum(pca.explained_variance_ratio_)))

# %%
# plot hypnogram (-1 = noise)
for i in np.arange(len(c)):
    if c[i]==-1:
        c[i] = c[i-1]
plt.figure(figsize=(10,6))
plt.plot(timestamp,c)
plt.title("Session ID-"+str(idx)+" "+sessionName+" TSNE+PCA="+str(pca_components)+" \n Possible Hypnogram - labels may not be in the correct order")


# %%
# comparison - only PCA
pca_2 = PCA(n_components=2)
pca_2_result = pca_2.fit_transform(samples)
print('Cumulative explained variation for 2 principal components: {}'.format(np.sum(pca_2.explained_variance_ratio_)))


clf = DBSCAN(eps=1,min_samples=25)
c_pca = clf.fit_predict(pca_2_result)
print(np.unique(c_pca))


df_pca = pd.DataFrame()
df_pca['PCA-2d-one'] = pca_2_result[:,0]
df_pca['PCA-2d-two'] = pca_2_result[:,1]
plt.figure(figsize=(16,10))
sns.scatterplot(
    x="PCA-2d-one", y="PCA-2d-two",
    hue = c_pca,
    palette=sns.color_palette("hls", len(np.unique(c_pca))),
    data=df_pca,
    legend="full",
    alpha=0.3
)
plt.title("Session ID-"+str(idx)+" "+sessionName+" PCA \n explained variation:"+str(np.sum(pca_2.explained_variance_ratio_)))


# %%
# plot hypnogram (-1 = noise)
for i in np.arange(len(c_pca)):
    if c_pca[i]==-1:
        c_pca[i] = c_pca[i-1]
        
plt.figure(figsize=(10,6))
plt.plot(timestamp,c_pca)
plt.title("Session ID-"+str(idx)+" "+sessionName+" PCA=2 \n Possible Hypnogram - labels may not be in the correct order")


# %%

#HEATMAP
samples1/=np.max(samples1)
samples1=(samples1+1)/2

fig = plt.figure(figsize=(8,4))
ax = fig.add_subplot(111)
ax.set_aspect(aspect=0.5)
plt.imshow(samples1[::10].T, cmap='hot', interpolation='nearest')
plt.show()

#%%

a = np.random.random((16, 16))
plt.imshow(a, cmap='hot', interpolation='nearest')

