## In this file we do EDA on sleep data using the CLuster Finder

# run folder is ML2020/code


# %%
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline


import os
import sys
sys.path.insert(0,os.path.join(os.getcwd(),"lib"))


from lib import Vizualization
from lib import ClusterFinder



# %%
# load sleep data from pkl file
infile = open('../data/df_features_test_env_unlabeled.pkl','rb')
obj = pickle.load(infile)
infile.close()


# %%
obj.head()
#obj.to_csv('sleep.csv')


# %%
ind = obj.index
ind.names


# %%
# get recording times
recs = ind.get_level_values(0)


# %%
sessionNames_list = obj.index.levels[0]
print(sessionNames_list)


# %%
# get samples from sessionName 'idx'
idx=6
sessionName = sessionNames_list[idx]
samples = obj.loc[sessionName].get_values()
timestamp = obj.index.levels[2][:len(samples)]

print(sessionName,samples.shape, timestamp.shape)


#%%

pipe = Pipeline(steps=[
    #('scalar', StandardScaler()),
    #('rpca', skRPCA(n_components=2)),
    ('pca', PCA(n_components=2)),
    ('model',GaussianMixture(n_components=5,covariance_type='full'))
])

model = ClusterFinder.ClusterFinder(pipe)

vizualization = Vizualization.Vizualization()


model.fit(samples)
optimal_k_bic = model.optimal_k_bic(samples,num_iterations=10)
optimal_k_silhouette = model.optimal_k_silhouette(samples,num_iterations=10)
optimal_k_distortions = model.optimal_k_distortions(samples)

print("bic:{},sil:{},dist:{}".format(optimal_k_bic,optimal_k_silhouette,optimal_k_distortions))

#%%

X_pca =model.just_transforms(samples) 

model.pipe['model'].n_components=optimal_k_bic
model.fit(samples)
y_bic = model.predict(samples)

plt.figure(figsize=(16,10)) 
vizualization.pca_visualization(X_pca,y_bic,title="Session ID-"+str(idx)+" "+sessionName+"PCA=2 + GMM + BIC",newplot=False)