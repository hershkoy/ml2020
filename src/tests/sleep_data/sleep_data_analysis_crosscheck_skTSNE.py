## In this file we do EDA on sleep data using the CLuster Finder

# run folder is ML2020/code


"""
This was an attempt to insert TSNE into the pipeline. 
Problem is that TSNE doesn't have fit method, only fit_transform, so didn't finish the code!!!!
"""



# %%
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
import umap.umap_ as umap


import os
import sys
sys.path.insert(0,os.path.join(os.getcwd(),"lib"))


import Vizualization
import ClusterFinder
import skRPCA
import skTSNE



# %%
# load sleep data from pkl file
infile = open('../data/df_features_test_env_unlabeled.pkl','rb')
obj = pickle.load(infile)
infile.close()


# %%
obj.head()
#obj.to_csv('sleep.csv')


# %%
ind = obj.index
ind.names


# %%
# get recording times
recs = ind.get_level_values(0)


# %%
sessionNames_list = obj.index.levels[0]
print(sessionNames_list)


# %%
# get samples from sessionName 'idx'
idx1=0
idx2=1

sessionName1 = sessionNames_list[idx1]
samples1 = obj.loc[sessionName1].get_values()
timestamp1 = obj.index.levels[2][:len(samples1)]

sessionName2 = sessionNames_list[idx2]
samples2 = obj.loc[sessionName2].get_values()
timestamp2 = obj.index.levels[2][:len(samples2)]

print(sessionName1,samples1.shape, timestamp1.shape)
print(sessionName2,samples2.shape, timestamp2.shape)


#%%

rpca_components = 121
pca_components = 10

pipe = Pipeline(steps=[
    #('scalar', StandardScaler()),
    ('rpca', skRPCA.skRPCA(n_components=rpca_components)),
    ('pca', PCA(n_components=pca_components)),
    ('tsne',skTSNE.skTSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)),
    ('model',GaussianMixture(n_components=5,covariance_type='full'))
])

model = ClusterFinder.ClusterFinder(pipe)

vizualization = Vizualization.Vizualization()

model.fit(samples1)


#%%

tsne_results =model.just_transforms(samples2) 

clf = DBSCAN(eps=0.4,min_samples=20)
c = clf.fit_predict(tsne_results)


# plot 2-D decomposition with clustering result
df = pd.DataFrame()
df['tsne-2d-one'] = tsne_results[:,0]
df['tsne-2d-two'] = tsne_results[:,1]
plt.figure(figsize=(16,10))
sns.scatterplot(
    x="tsne-2d-one", y="tsne-2d-two",
    hue = c,
    palette=sns.color_palette("hls", len(np.unique(c))),
    data=df,
    legend="full",
    alpha=0.3
)
plt.title("fit on ID#-"+str(idx1)+", crosscheck on ID#"+str(idx2)+"\n  RPCA="+str(rpca_components)+" PCA="+str(pca_components)+"+TSNE ")

plt.savefig("images/crosscheck_"+str(idx1)+"#"+str(idx2)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+"+TSNE.png")

#%%

# plot hypnogram (-1 = noise)
for i in np.arange(len(c)):
    if c[i]==-1:
        c[i] = c[i-1]
plt.figure(figsize=(10,6))
plt.plot(timestamp2,c)
plt.title("fit on ID#-"+str(idx1)+", crosscheck on ID#"+str(idx2)+"\n RPCA="+str(rpca_components)+" PCA="+str(pca_components)+"+TSNE \n Possible Hypnogram - labels may not be in the correct order")

plt.savefig("images/crosscheck_"+str(idx1)+"#"+str(idx2)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+"+TSNE_hipnogram.png")

#%%

# color based on time


plt.figure(figsize=(16,10))

color_ts = np.arange(len(tsne_results))


plt.scatter(tsne_results[:,0], tsne_results[:,1], alpha = .3, c = color_ts, cmap = 'copper')
cbar = plt.colorbar()

plt.title("fit on ID#-"+str(idx1)+", crosscheck on ID#"+str(idx2)+"\n color by time \n  RPCA="+str(rpca_components)+" PCA="+str(pca_components)+"+TSNE ")

plt.savefig("images/crosscheck_"+str(idx1)+"#"+str(idx2)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+"+TSNE_timecolor.png")


#%%

# UMAP

reducer  = umap.UMAP(n_neighbors=10, min_dist=0.1, metric="euclidean") #those are deafult params in the package
umap_results = reducer.fit_transform(X_pca)

clf = DBSCAN(eps=0.4,min_samples=20)
c = clf.fit_predict(umap_results)

# plot 2-D decomposition with clustering result
df = pd.DataFrame()
df['umap-2d-one'] = umap_results[:,0]
df['umap-2d-two'] = umap_results[:,1]
plt.figure(figsize=(16,10))
sns.scatterplot(
    x="umap-2d-one", y="umap-2d-two",
    hue = c,
    palette=sns.color_palette("hls", len(np.unique(c))),
    data=df,
    legend="full",
    alpha=0.3
)
plt.title("fit on ID#-"+str(idx1)+", crosscheck on ID#"+str(idx2)+"\n RPCA="+str(rpca_components)+"+PCA="+str(pca_components)+"+UMAP ")

plt.savefig("images/crosscheck_"+str(idx1)+"#"+str(idx2)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+"+UMAP.png")

#%% 

# plot hypnogram (-1 = noise)
for i in np.arange(len(c)):
    if c[i]==-1:
        c[i] = c[i-1]
plt.figure(figsize=(10,6))
plt.plot(timestamp2,c)
plt.title("fit on ID#-"+str(idx1)+", crosscheck on ID#"+str(idx2)+"\n RPCA="+str(rpca_components)+" PCA="+str(pca_components)+"+UMAP \n Possible Hypnogram - labels may not be in the correct order")

plt.savefig("images/crosscheck_"+str(idx1)+"#"+str(idx2)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+"+UMAP_hipnogram.png")


#%%

# UMAP color based on time


plt.figure(figsize=(16,10))

color_ts = np.arange(len(umap_results))

plt.scatter(umap_results[:,0], umap_results[:,1], alpha = .3, c = color_ts, cmap = 'copper')
cbar = plt.colorbar()

plt.title("fit on ID#-"+str(idx1)+", crosscheck on ID#"+str(idx2)+"\n color by time \n  RPCA="+str(rpca_components)+" PCA="+str(pca_components)+"+UMAP ")

plt.savefig("images/crosscheck_"+str(idx1)+"#"+str(idx2)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+"+UMAP_timecolor.png")
