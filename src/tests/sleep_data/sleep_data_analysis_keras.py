## In this file we do EDA on sleep data using the CLuster Finder

# run folder is ML2020/code


# %%

import mlflow

import sklearn
from sklearn.preprocessing import FunctionTransformer
from sklearn.metrics import mean_squared_error,adjusted_rand_score

import tensorflow as tf
import numpy as np
import pandas as pd

import os
import sys
import glob
import matplotlib.pyplot as plt

sys.path.insert(0,os.path.join(os.getcwd(),"lib"))

import keras
import keras.backend as K

import Vizualization
import experiment_utils as utils

viz = Vizualization.Vizualization()


# %%

# load data

df_labeled = utils.load_data('../data/df_features_test_env_labeled.pkl')
labeled_data = utils.load_data('../data/df_labels_test_env.pkl')

ind = df_labeled.index
sessionNames_list = df_labeled.index.levels[0]
print(sessionNames_list)

window_size = 15


#%%

x_data = np.empty((0, window_size, 122), int)
y_data = np.empty((0, 1), int)


for idx in range(len(sessionNames_list)):

    sessionName = sessionNames_list[idx]
    print(f"sessionName:{sessionName}", end =" ") 

    samples = utils.filter_by(df_labeled,{'sessionName' : [sessionName]})
    timestamp = samples.reset_index().timestamp
    samples = samples.get_values()
    samples = pd.DataFrame(data=samples,index=timestamp)

    print(f"data:{samples.shape}", end =" ") 
    

    labels = utils.filter_by(labeled_data,{'sessionName' : [sessionName]}).reset_index()
    hypno = labels.y
    hypno_ts = labels.timestamp
    hypno.index = list(labels.timestamp)

    hypno_val = hypno.replace(['U','W','R','N2','N3'],[0,1,2,3,4])
   
    print(f"hypno_val:{len(hypno_val)}")

    start = np.round((hypno_ts[0]-timestamp[0])/(window_size*1000)).astype(int)
    if (start>0):    
        samples=samples[start:]
        
    if (samples.shape[0]/window_size!=0):
        end = int(samples.shape[0]/window_size)*window_size
        samples=samples[:end]
        
    samples['ind'] = np.arange(samples.shape[0])/10000 #10K, just a big number
    samples = samples.to_numpy()
    samples = samples.reshape(int(samples.shape[0]/window_size),window_size,122)
    
    if (samples.shape[0]>hypno_val.shape[0]):
        end = hypno_val.shape[0]
        samples=samples[:end]
        
    x_data = np.vstack((x_data,samples))
    y_data = np.vstack((y_data,hypno_val.to_numpy().reshape(-1, 1)))
    
    print(f"s0:{samples.shape}")
    
    

#%%

n_classes = len([0,1,2,3,4])

tf.compat.v1.keras.backend.clear_session()

inputs = tf.keras.layers.Input((window_size, 122))

conv1 =  tf.keras.layers.Conv1D(16, kernel_size=8, padding='SAME')
res = conv1(inputs)
res = tf.keras.layers.LSTM(32)(res)
res = tf.keras.layers.Dense(32, activation=tf.nn.leaky_relu)(res)
res = tf.keras.layers.Dense(n_classes, activation=tf.nn.softmax)(res)

model = tf.keras.models.Model(inputs, res)
model.compile(optimizer='rmsprop', loss='sparse_categorical_crossentropy')
model.fit(x_data, y_data, batch_size=4, epochs=5)

