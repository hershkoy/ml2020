## In this file I run clusterring and check how the labels look on that

# run folder is ML2020/code


# %%


import skRPCA
import ClusterFinder
import Vizualization


import mlflow

import sklearn
import numpy as np
from sklearn.preprocessing import FunctionTransformer
import umap.umap_ as umap
import pandas as pd

import os
import sys
import glob
import matplotlib.pyplot as plt


sys.path.insert(0,os.path.join(os.getcwd(),"lib"))

import experiment_utils as utils


viz = Vizualization.Vizualization()


# %%

# load data

df_labeled = utils.load_data('../data/df_features_test_env_labeled.pkl')
labeled_data = utils.load_data('../data/df_labels_test_env.pkl')

ind = df_labeled.index
sessionNames_list = df_labeled.index.levels[0]
print(sessionNames_list)


# %%
# get samples from sessionName 'idx'

experiment_name = "labelled"
image_dir = "images_tmp"

hypnogram_smoothing = 30 

idx = 1

sessionName = sessionNames_list[idx]

samples = utils.filter_by(df_labeled,{'sessionName' : [sessionName]})
timestamp = samples.reset_index().timestamp
samples = samples.get_values()
samples = pd.DataFrame(data=samples,index=timestamp)

labels = utils.filter_by(labeled_data,{'sessionName' : [sessionName]}).reset_index()
hypno = labels.y
hypno_ts = labels.timestamp
hypno.index = list(labels.timestamp)
hypno_val = hypno.replace(['U','W','R','N2','N3'],[0,1,2,3,4])

lables_upSmpl = list(np.repeat(list(hypno_val), 15))

print(sessionName, samples.shape, timestamp.shape)


#%%


model_config = {
    'n_rpca_components': 121,
    'n_pca_components': 10,
    'umap_n_neighbors': 10,
    'umap_min_dist': 0.1,
    'umap_metric': 'euclidean',
    'dbscan_eps': 0.4,
    'dbscan_min_samples': 20,
    'remove_first_dim': False
}

model = ClusterFinder.ClusterFinder(pipe=None,config=model_config)
pipe = model.getPipe()


# %%


model.fit(samples)

umap_results = model.special_fit_transforms(samples)

clf = pipe['model']
predicted = clf.fit_predict(umap_results)

#%%

title = ("model with human labels \n"
         + "Session ID-" + str(idx) + f"{sessionName} \n"
         + model.getModelStr())

viz.plot_umap(umap_results[:len(lables_upSmpl)],list(lables_upSmpl))
