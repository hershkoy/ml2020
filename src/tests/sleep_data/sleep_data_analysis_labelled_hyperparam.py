## In this file we do EDA on sleep data using the CLuster Finder

# run folder is ML2020/code


# %%

import mlflow

import sklearn
from sklearn.preprocessing import FunctionTransformer
from sklearn.metrics import mean_squared_error,adjusted_rand_score
import random
import itertools

import umap.umap_ as umap

import os
import sys
import glob
import matplotlib.pyplot as plt

sys.path.insert(0,os.path.join(os.getcwd(),"lib"))

import skRPCA
import ClusterFinder
import Vizualization
import experiment_utils as utils

viz = Vizualization.Vizualization()


def all_but_first_column(X):
    return X[:, 1:]


# %%

# load data

df = utils.load_data('../data/df_features_test_env_labeled.pkl')
labeled_data = utils.load_data('../data/df_labels_test_env.pkl')

ind = df.index
sessionNames_list = df.index.levels[0]
print(sessionNames_list)


# %%
# get samples from sessionName 'idx'

experiment_name = "labelled"
image_dir = "images_tmp"

hypnogram_smoothing = 30 





#%%


cand = [[0,1,2,3,4,5],
        [0,121],         
        [10,30,50],
        [10,16,24,30], #,36,42,50,100,200],
        [0.0, 0.1, 0.25, 0.5, 0.8, 0.99],
        ['euclidean'], #['euclidean','manhattan','chebyshev','minkowski'],
        [False,True]]

cand = [[0],
        [121],         
        [30],
        [30], #,36,42,50,100,200],
        [0.5],
        ['minkowski'],
        [True]]


cand_list = list(itertools.product(*cand))
random.shuffle(cand_list)

for i in range(len(cand_list)):

    print(f"run #{i}")
    
    cand_o = cand_list[i]
    
    idx = cand_o[0]
    
    sessionName = sessionNames_list[idx]
    
    samples = utils.filter_by(df,{'sessionName' : [sessionName]})
    timestamp = samples.reset_index().timestamp
    samples = samples.get_values()
    
    labels = utils.filter_by(labeled_data,{'sessionName' : [sessionName]}).reset_index()
    hypno = labels.y
    hypno_ts = labels.timestamp
    hypno.index = list(labels.timestamp)
    
    hypno_val = hypno.replace(['U','W','R','N2','N3'],[-1,0,1,2,3])

    cand_o = cand_list[i]
    
    n_rpca_components = cand_o[1]
    n_pca_components = cand_o[2]
    umap_n_neighbors = cand_o[3]
    umap_min_dist = cand_o[4]
    umap_metric = cand_o[5]
    remove_first_dim = cand_o[6]
    dbscan_eps = 0.4
    dbscan_min_samples = 20

    steps = []
    if (n_rpca_components>0):
        steps.append(('rpca', skRPCA.skRPCA(n_components=n_rpca_components)))
    steps.append(('pca', utils.PCA(n_components=n_pca_components)))
    if (remove_first_dim):
        steps.append(('remove_dimension', FunctionTransformer(all_but_first_column)))
    steps.append(('reduce', umap.UMAP(n_neighbors=umap_n_neighbors, min_dist=umap_min_dist, metric=umap_metric, verbose=True)))
    steps.append(('model', utils.DBSCAN(eps=dbscan_eps, min_samples=dbscan_min_samples)))
    print(cand_o)

    pipe = sklearn.pipeline.Pipeline(steps=steps)

    model = ClusterFinder.ClusterFinder(pipe)
    umap_results = model.special_fit_transforms(samples)
    print("after transform")
    clf = pipe['model']
    predicted = clf.fit_predict(umap_results)
    print("after predict")


    for f in glob.glob(image_dir + '/*'):
        os.remove(f)
    
    mlflow.set_experiment(experiment_name)
    
    remove_pca0 = 'remove_dimension' in [i[0] for i in pipe.steps]
    
    mlflow.start_run()
    mlflow.log_params({
        'test_id': idx,
        'n_rpca_components': n_pca_components,
        'umap_n_neighbors': umap_n_neighbors,
        'umap_min_dist': umap_min_dist,
        "umap_metric": umap_metric,
        "remove_pca0": remove_pca0,
        'dbscan_eps': dbscan_eps,
        'dbscan_min_samples': dbscan_min_samples,
    })

    ##############
    # UMAP regular
    ##############
    
    title = ("Session ID-"
             + str(idx)
             + " " + sessionName
             + " RPCA=" + str(n_rpca_components)
             + "+PCA=" + str(n_pca_components)
             + "+UMAP ")
    
    img_fname = (image_dir + "/"
                + "s"+str(idx)
                +"_RPCA=" + str(n_rpca_components) 
                + "_PCA=" + str(n_pca_components) 
                + "+UMAP.png")
    
    viz.plot_umap(umap_results, predicted, title=title, fname=img_fname)
    
    ##############
    # Hypnogram
    ##############
    
    title = ("Session ID-" 
             + str(idx) + " " + sessionName
             + " RPCA=" + str(n_rpca_components)
             + " PCA=" + str(n_pca_components) 
             + "+UMAP \n"
             + " Possible Hypnogram - labels may not be in the correct order")
    
    img_fname = (image_dir + "/"
                 + "s" + str(idx) 
                 + "_RPCA=" + str(n_rpca_components)
                 + "_PCA=" + str(n_pca_components)
                 + "+UMAP_hipnogram.png")
                 
    viz.plot_hypnogram(predicted, title=title, fname=img_fname)
    
    
    
    ##############
    # Time Color
    ##############
    
    title = ("Session ID-" 
             + str(idx) + " " + sessionName
             + "\n color by time \n"
             + "RPCA=" + str(n_rpca_components)
             + " PCA=" + str(n_pca_components) 
             + "+UMAP ")
    
    img_fname = (image_dir + "/"
                 + "s" + str(idx) 
                 + "_RPCA=" + str(n_rpca_components)
                 + "_PCA=" + str(n_pca_components)
                 + "+UMAP_timecolor.png")
    
    viz.plot_timecolor(umap_results, predicted, title=title, fname=img_fname)
    
    predicted_fix = utils.adjust_hypno(hypno_val,predicted,timestamp)

    try:    
        score_rand = adjusted_rand_score(hypno_val,predicted_fix)
    except:
        score_rand = -999
    

    mlflow.log_metrics({'rand': score_rand})
    
    break


    ##############
    # Cleanup
    ##############

    del pipe
    del model
    del umap_results
    del clf
    del predicted
    plt.close("all")    


    mlflow.log_artifacts(image_dir, "images")
    
    mlflow.end_run()


