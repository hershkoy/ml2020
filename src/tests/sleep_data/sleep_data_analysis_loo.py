import mlflow

import sklearn
from sklearn.preprocessing import FunctionTransformer
import umap.umap_ as umap

import os
import sys
import glob
import matplotlib.pyplot as plt
import numpy as np

sys.path.insert(0,os.path.join(os.getcwd(),"lib"))


import skRPCA
import ClusterFinder
import Vizualization
import experiment_utils as utils

viz = Vizualization.Vizualization()


df = utils.load_data('../data/df_features_test_env_unlabeled.pkl')

ind = df.index
ind.names
recs = ind.get_level_values(0)
sessionNames_list = df.index.levels[0]
print(sessionNames_list)

#%%

experiment_name = "leave_one_out"
image_dir = "images_tmp"

hypnogram_smoothing = 30

n_rpca_components = 121
n_pca_components = 10
umap_n_neighbors = 30
umap_min_dist = 0.1
umap_metric = 'euclidean'
dbscan_eps = 0.4
dbscan_min_samples = 20

def all_but_first_column(X):
    return X[:, 1:]


pipe = sklearn.pipeline.Pipeline(steps=[
    # ('scalar', StandardScaler()),
    ('rpca', skRPCA.skRPCA(n_components=n_rpca_components)),
    ('pca', utils.PCA(n_components=n_pca_components)),
    ('remove_dimension', FunctionTransformer(all_but_first_column)),
    ('reduce', umap.UMAP(n_neighbors=umap_n_neighbors, min_dist=umap_min_dist, metric=umap_metric,verbose=True)),
    ('model', utils.DBSCAN(eps=dbscan_eps, min_samples=dbscan_min_samples)),
])

model = ClusterFinder.ClusterFinder(pipe)


# %%

dat_id = [0,1,2,3,4,5,6]

test_id = 5
train_id = dat_id.copy()
train_id.remove(test_id)

samples_train = np.zeros((1,121))
for idx in train_id:
    sessionName = sessionNames_list[idx]
    samples = df.loc[sessionName].to_numpy()
    samples_train = np.concatenate((samples_train,samples),axis=0)

idx=test_id
sessionName = sessionNames_list[idx]
samples_test = df.loc[sessionName].to_numpy()
    
umap_results_train = model.special_fit_transforms(samples_train)
print("after transform1")

umap_results_test = model.just_transforms(samples_test)


clf = pipe['model']
predicted = clf.fit_predict(umap_results_test)
print("after predict")


# %%

for f in glob.glob(image_dir + '/*'):
    os.remove(f)

mlflow.set_experiment(experiment_name)

remove_pca0 = 'remove_dimension' in [i[0] for i in pipe.steps]

mlflow.start_run()
mlflow.log_params({
    'n_rpca_components': n_pca_components,
    'umap_n_neighbors': umap_n_neighbors,
    'umap_min_dist': umap_min_dist,
    "umap_metric": umap_metric,
    "remove_pca0": remove_pca0,
    'dbscan_eps': dbscan_eps,
    'dbscan_min_samples': dbscan_min_samples,
})

##train 

train_ids_str = "".join([str(i) for i in train_id])
title = ('UMAP leave one out: training on datastes '+train_ids_str)

img_fname = (image_dir + "/"
            + "loo_train_"+train_ids_str+".png")

viz.plot_umap(umap_results_train,title=title, fname=img_fname)


#apply

title = ('UMAP leave one out: test on session '+str(idx))

img_fname = (image_dir + "/"
            + "loo_test"+str(idx)+".png")

viz.plot_umap(umap_results_test, title=title, fname=img_fname)


##############
# Hypnogram (smoothed)
##############

title = ("UMAP leave one out: training on datastes '+train_ids_str \n"
         + " Possible Hypnogram. SMOOTHED (window=" + str(hypnogram_smoothing) + ")\n"
         + "-labels may not be in the correct order")

img_fname = (image_dir + "/"
            + "loo_train_"+train_ids_str
             + "_hipnogram_smth" + str(hypnogram_smoothing) + ".png")
             
viz.plot_hypnogram(predicted, smooth=hypnogram_smoothing, title=title, fname=img_fname)



mlflow.log_artifacts(image_dir, "images")

mlflow.end_run()
