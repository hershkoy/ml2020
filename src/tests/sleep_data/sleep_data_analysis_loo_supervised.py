import mlflow

import sklearn
from sklearn.preprocessing import FunctionTransformer
from sklearn.metrics import mean_squared_error,adjusted_rand_score

import umap.umap_ as umap

import os
import sys
import glob
import matplotlib.pyplot as plt
import numpy as np
import random
import itertools

sys.path.insert(0,os.path.join(os.getcwd(),"lib"))


import skRPCA
import ClusterFinder
import Vizualization
import experiment_utils as utils

viz = Vizualization.Vizualization()


df = utils.load_data('../data/df_features_test_env_unlabeled.pkl')

ind = df.index
ind.names
recs = ind.get_level_values(0)
sessionNames_list = df.index.levels[0]
print(sessionNames_list)

#%%

def all_but_first_column(X):
    return X[:, 1:]

experiment_name = "leave_one_out"
image_dir = "images_tmp"
dat_id = [0,1,2,3,4,5,6]

hypnogram_smoothing = 30

test_id = 5


cand = [[0,121],         
        [10,30,50],
        [10,16,24,30], #,36,42,50,100,200],
        [0.0, 0.1, 0.25, 0.5, 0.8, 0.99],
        ['euclidean'], #['euclidean','manhattan','chebyshev','minkowski'],
        [False,True]]

cand_list = list(itertools.product(*cand))
random.shuffle(cand_list)

base_n_rpca_components = 121
base_n_pca_components = 50
base_umap_n_neighbors = 30
base_umap_min_dist = 0.1
base_umap_metric = 'euclidean'
base_dbscan_eps = 0.4
base_dbscan_min_samples = 20


base_pipe = sklearn.pipeline.Pipeline(steps=[
    # ('scalar', StandardScaler()),
    ('rpca', skRPCA.skRPCA(n_components=base_n_rpca_components)),
    ('pca', utils.PCA(n_components=base_n_pca_components)),
    ('remove_dimension', FunctionTransformer(all_but_first_column)),
    ('reduce', umap.UMAP(n_neighbors=base_umap_n_neighbors, min_dist=base_umap_min_dist, metric=base_umap_metric,verbose=True)),
    ('model', utils.DBSCAN(eps=base_dbscan_eps, min_samples=base_dbscan_min_samples)),
])

base_model = ClusterFinder.ClusterFinder(base_pipe)

idx=test_id
sessionName = sessionNames_list[idx]
samples_test = df.loc[sessionName].to_numpy()

base_umap_results = base_model.special_fit_transforms(samples_test,max_depth=-1)

base_clf = base_pipe['model']
base_predicted = base_clf.fit_predict(base_umap_results)

viz.plot_umap(base_umap_results, base_predicted, title=f"base umap session {test_id}")
base_hypno_smth = viz.plot_hypnogram(base_predicted, smooth=hypnogram_smoothing, title=f"base hypnogram session {test_id}")



#%%

train_id = dat_id.copy()
train_id.remove(test_id)

samples_train = np.zeros((1,121))
for idx in train_id:
    sessionName = sessionNames_list[idx]
    samples = df.loc[sessionName].to_numpy()
    samples_train = np.concatenate((samples_train,samples),axis=0)

print(f"s1:{len(samples_train)}")
    

for i in range(len(cand_list)):
    
   
    print(f"run #{i}")

    cand_o = cand_list[i]
    
    n_rpca_components = cand_o[0]
    n_pca_components = cand_o[1]
    umap_n_neighbors = cand_o[2]
    umap_min_dist = cand_o[3]
    umap_metric = cand_o[4]
    remove_first_dim = cand_o[5]
    dbscan_eps = 0.4
    dbscan_min_samples = 20
  
    def all_but_first_column(X):
        return X[:, 1:]

    steps = []
    if (n_rpca_components>0):
        steps.append(('rpca', skRPCA.skRPCA(n_components=n_rpca_components)))
    steps.append(('pca', utils.PCA(n_components=n_pca_components)))
    if (remove_first_dim):
        steps.append(('remove_dimension', FunctionTransformer(all_but_first_column)))
    steps.append(('reduce', umap.UMAP(n_neighbors=umap_n_neighbors, min_dist=umap_min_dist, metric=umap_metric, verbose=True)))
    steps.append(('model', utils.DBSCAN(eps=dbscan_eps, min_samples=dbscan_min_samples)))
    print(cand_o)

    pipe = sklearn.pipeline.Pipeline(steps=steps)

    model = ClusterFinder.ClusterFinder(pipe)
   
    umap_results_train = model.special_fit_transforms(samples_train)    
    print("after transform")

    umap_results_test = model.just_transforms(samples_test)

    clf = pipe['model']
    predicted = clf.fit_predict(umap_results_test)
    print("after predict")

    for f in glob.glob(image_dir + '/*'):
        os.remove(f)

    mlflow.set_experiment(experiment_name)

    remove_pca0 = 'remove_dimension' in [i[0] for i in pipe.steps]

    mlflow.start_run()
    mlflow.log_params({
        'test_id': test_id,
        'n_rpca_components': n_rpca_components,
        'n_pca_components': n_pca_components,
        'umap_n_neighbors': umap_n_neighbors,
        'umap_min_dist': umap_min_dist,
        "umap_metric": umap_metric,
        "remove_pca0": remove_pca0,
        #'dbscan_eps': dbscan_eps,
        #'dbscan_min_samples': dbscan_min_samples,
    })

    params_log = ""
    if (n_rpca_components>0):
        params_log =params_log+f"rpca{n_rpca_components}_"
    params_log =params_log+f"pca{n_pca_components}_"
    if (remove_pca0):
        params_log =params_log+"rm0_"
    params_log =params_log+f"umap{umap_n_neighbors}-{umap_min_dist}-{umap_metric}"

    train_ids_str = "".join([str(i) for i in train_id])


    ##train 

    title = ('UMAP leave one out: training on datastes '+train_ids_str+"\n"+params_log)

    img_fname = (image_dir + "/"
                + "loo_train_"+train_ids_str+"_"+params_log+".png")

    viz.plot_umap(umap_results_train,title=title, fname=img_fname)


    #apply

    title = ('UMAP leave one out: test on session '+str(idx)+"\n"+params_log)

    img_fname = (image_dir + "/"
                + "loo_test"+str(idx)+"_"+params_log+".png")

    viz.plot_umap(umap_results_test, title=title, fname=img_fname)


    ##############
    # Hypnogram (smoothed)
    ##############

    title = ("UMAP leave one out: training on datastes "+train_ids_str+"\n"+params_log+"\n"+
              " Possible Hypnogram. SMOOTHED (window=" + str(hypnogram_smoothing) + ")\n"+
              "-labels may not be in the correct order")

    img_fname = (image_dir + "/"
                + "loo_train_"+train_ids_str+"_"+params_log
                 + "_hipnogram_smth" + str(hypnogram_smoothing) + ".png")

    test_hypno_smth = viz.plot_hypnogram(predicted, smooth=hypnogram_smoothing, title=title, fname=img_fname)
    
    score = mean_squared_error(base_hypno_smth/np.max(base_hypno_smth),test_hypno_smth/np.max(test_hypno_smth))
    
    mlflow.log_metrics({'error': score})




    ##############
    # Cleanup
    ##############

    del pipe
    del model
    del umap_results_train
    del umap_results_test
    del clf
    del predicted


    mlflow.log_artifacts(image_dir, "images")

    mlflow.end_run()
    
#%%
    
a = utils.load_data('../data/predicted_crash.pickle')
c=a
smooth=30

def rolling_window(a, window):
    a = np.concatenate((np.array([0]*(window-1)),a))
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    
for i in np.arange(len(c)):
    if c[i]==-1:
        c[i] = c[i-1]
        
c_smoothed = np.zeros_like(c)
window_size = smooth
i=0
for window in rolling_window(c, window_size):
    hist,edges = np.histogram(window)
    most_freq = np.round(edges[np.argmax(hist)]).astype(int)
    c_smoothed[i] = most_freq
    i=i+1
c=c_smoothed  


