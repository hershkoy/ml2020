## In this file we do EDA on sleep data using the CLuster Finder

# run folder is ML2020/code


# %%

import skRPCA
import ClusterFinder
import Vizualization

import mlflow

import sklearn
from sklearn.preprocessing import FunctionTransformer
import umap.umap_ as umap

import os
import sys
import glob
import matplotlib.pyplot as plt

import experiment_utils as utils

sys.path.insert(0,os.path.join(os.getcwd(),"lib"))

viz = Vizualization.Vizualization()


# %%

# load data

obj = utils.load_data('../data/df_features_test_env_unlabeled.pkl')

ind = obj.index
ind.names
recs = ind.get_level_values(0)
sessionNames_list = obj.index.levels[0]
print(sessionNames_list)


# %%
# get samples from sessionName 'idx'

experiment_name = "experiment 1"
image_dir = "images_tmp"

hypnogram_smoothing = 30 

idx = 5
n_rpca_components = 121
n_pca_components = 10
umap_n_neighbors = 10
umap_min_dist = 0.1
umap_metric = 'euclidean'
dbscan_eps = 0.4
dbscan_min_samples = 20

sessionName = sessionNames_list[idx]
samples = obj.loc[sessionName].get_values()
timestamp = obj.index.levels[2][:len(samples)]

print(sessionName, samples.shape, timestamp.shape)

def all_but_first_column(X):
    return X[:, 1:]

pipe = sklearn.pipeline.Pipeline(steps=[
    # ('scalar', StandardScaler()),
    ('rpca', skRPCA.skRPCA(n_components=n_rpca_components)),
    ('pca', utils.PCA(n_components=n_pca_components)),
    ('remove_dimension', FunctionTransformer(all_but_first_column)),
    ('reduce', umap.UMAP(n_neighbors=umap_n_neighbors, min_dist=umap_min_dist, metric=umap_metric)),
    ('model', utils.DBSCAN(eps=dbscan_eps, min_samples=dbscan_min_samples)),
])


# %%

""
model = ClusterFinder.ClusterFinder(pipe)

umap_results = model.special_fit_transforms(samples)

clf = pipe['model']
predicted = clf.fit_predict(umap_results)

# %%

for f in glob.glob(image_dir + '/*'):
    os.remove(f)

mlflow.set_experiment(experiment_name)

remove_pca0 = 'remove_dimension' in [i[0] for i in pipe.steps]

mlflow.start_run()
mlflow.log_params({
    'n_rpca_components': n_pca_components,
    'umap_n_neighbors': umap_n_neighbors,
    'umap_min_dist': umap_min_dist,
    "umap_metric": umap_metric,
    "remove_pca0": remove_pca0,
    'dbscan_eps': dbscan_eps,
    'dbscan_min_samples': dbscan_min_samples,
})

##############
# UMAP regular
##############

title = ("Session ID-"
         + str(idx)
         + " " + sessionName
         + " RPCA=" + str(n_rpca_components)
         + "+PCA=" + str(n_pca_components)
         + "+UMAP ")

img_fname = (image_dir + "/"
            + "s"+str(idx)
            +"_RPCA=" + str(n_rpca_components) 
            + "_PCA=" + str(n_pca_components) 
            + "+UMAP.png")

viz.plot_umap(umap_results, predicted, title=title, fname=img_fname)

##############
# Hypnogram
##############

title = ("Session ID-" 
         + str(idx) + " " + sessionName
         + " RPCA=" + str(n_rpca_components)
         + " PCA=" + str(n_pca_components) 
         + "+UMAP \n"
         + " Possible Hypnogram - labels may not be in the correct order")

img_fname = (image_dir + "/"
             + "s" + str(idx) 
             + "_RPCA=" + str(n_rpca_components)
             + "_PCA=" + str(n_pca_components)
             + "+UMAP_hipnogram.png")
             
viz.plot_hypnogram(predicted, title=title, fname=img_fname)



##############
# Time Color
##############

title = ("Session ID-" 
         + str(idx) + " " + sessionName
         + "\n color by time \n"
         + "RPCA=" + str(n_rpca_components)
         + " PCA=" + str(n_pca_components) 
         + "+UMAP ")

img_fname = (image_dir + "/"
             + "s" + str(idx) 
             + "_RPCA=" + str(n_rpca_components)
             + "_PCA=" + str(n_pca_components)
             + "+UMAP_timecolor.png")

viz.plot_timecolor(umap_results, predicted, title=title, fname=img_fname)



##############
# Hypnogram (smoothed)
##############

title = ("Session ID-" 
         + str(idx) + " " + sessionName
         + " RPCA=" + str(n_rpca_components)
         + " PCA=" + str(n_pca_components) 
         + "+UMAP \n"
         + " Possible Hypnogram. SMOOTHED (window=" + str(hypnogram_smoothing) + ")\n"
         + "-labels may not be in the correct order")

img_fname = (image_dir + "/"
             + "s" + str(idx) 
             + "_RPCA=" + str(n_rpca_components)
             + "_PCA=" + str(n_pca_components)
             + "+UMAP_"
             + "hipnogram_smth" + str(hypnogram_smoothing) + ".png")
             
viz.plot_hypnogram(predicted, smooth=hypnogram_smoothing, title=title, fname=img_fname)

##############
# UMAP (enumerated)
##############

title = ("Session ID-"
         + str(idx)
         + " " + sessionName
         + " RPCA=" + str(n_rpca_components)
         + "+PCA=" + str(n_pca_components)
         + "+UMAP (clusters enumerated) ")

img_fname = (image_dir + "/"
            + "s"+str(idx)
            +"_RPCA=" + str(n_rpca_components) 
            + "_PCA=" + str(n_pca_components) 
            + "+UMAP_enum.png")

viz.plot_umap(umap_results, predicted, enumerated=True, title=title, fname=img_fname)


############################
# clusters concentration
############################

title = ("Session ID-"
         + str(idx)
         + " " + sessionName
         + " RPCA=" + str(n_rpca_components)
         + "+PCA=" + str(n_pca_components)
         + "+UMAP (clusters enumerated) ")

img_fname = (image_dir + "/"
            + "s"+str(idx)
            +"_RPCA=" + str(n_rpca_components) 
            + "_PCA=" + str(n_pca_components) 
            + "+UMAP_enum.png")

viz.plot_umap(umap_results, predicted, enumerated=True, title=title, fname=img_fname)



mlflow.log_artifacts(image_dir, "images")

mlflow.end_run()


#%%


ds_samples = samples[::30,:]
ds_model = ClusterFinder.ClusterFinder(pipe)
ds_umap_results = ds_model.special_fit_transforms(ds_samples)
ds_clf = pipe['model']
ds_clf.eps=1.2
ds_predicted = ds_clf.fit_predict(ds_umap_results)

#%%

############################
# compare to downsampled
############################


plt.figure(figsize=(16, 16))

ax = plt.subplot(2, 2, 1)
viz.plot_umap(umap_results, predicted, title="umap full data", enumerated=True,ax=ax)
ax =plt.subplot(2, 2, 2)
viz.plot_hypnogram(predicted, smooth=hypnogram_smoothing, title="hypnogram full data.  SMOOTHED (window=" + str(hypnogram_smoothing) + ")",ax=ax)

ax =plt.subplot(2, 2, 3)
viz.plot_umap(ds_umap_results, ds_predicted, title="umap downsampled", enumerated=True,ax=ax)
ax =plt.subplot(2, 2, 4)
viz.plot_hypnogram(ds_predicted, title="hypnogram downsampled",ax=ax)

plt.suptitle("Session ID-"
         + str(idx)
         + " " + sessionName)

plt.show()


