## In this file we do EDA on sleep data using the CLuster Finder

# run folder is ML2020/code


# %%
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
import umap.umap_ as umap

from skimage.measure import block_reduce

import os
import sys
sys.path.insert(0,os.path.join(os.getcwd(),"lib"))


import Vizualization
import ClusterFinder
import skRPCA



# %%
# load sleep data from pkl file
infile = open('../data/df_features_test_env_unlabeled.pkl','rb')
obj = pickle.load(infile)
infile.close()

ind = obj.index
ind.names
recs = ind.get_level_values(0)

sessionNames_list = obj.index.levels[0]
print(sessionNames_list)


# %%
# get samples from sessionName 'idx'
idx=5
sessionName = sessionNames_list[idx]
samples = obj.loc[sessionName].get_values()
timestamp = obj.index.levels[2][:len(samples)]

print(sessionName,samples.shape, timestamp.shape)


#%%

rpca_components = 121
pca_components = 10

pipe = Pipeline(steps=[
    #('scalar', StandardScaler()),
    ('rpca', skRPCA.skRPCA(n_components=rpca_components)),
    ('pca', PCA(n_components=pca_components)),
    ('model',GaussianMixture(n_components=5,covariance_type='full'))
])

model = ClusterFinder.ClusterFinder(pipe)

vizualization = Vizualization.Vizualization()

model.fit(samples)


#%%

X_pca =model.just_transforms(samples) 


#%%

# UMAP

reducer  = umap.UMAP(n_neighbors=30, min_dist=0, metric="euclidean",verbose=True) #those are deafult params in the package
umap_results = reducer.fit_transform(X_pca[1:])

clf = DBSCAN(eps=0.4,min_samples=20)
c = clf.fit_predict(umap_results)

# plot 2-D decomposition with clustering result
df = pd.DataFrame()
df['umap-2d-one'] = umap_results[:,0]
df['umap-2d-two'] = umap_results[:,1]
plt.figure(figsize=(16,10))
sns.scatterplot(
    x="umap-2d-one", y="umap-2d-two",
    hue = c,
    palette=sns.color_palette("hls", len(np.unique(c))),
    data=df,
    legend="full",
    alpha=0.3
)
plt.title("Session ID-"+str(idx)+" "+sessionName+" RPCA="+str(rpca_components)+"+PCA="+str(pca_components)+"+UMAP ")

#plt.savefig("images/s"+str(idx)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+"+UMAP.png")

#%%

plt.figure(figsize=(8,5))

# plot hypnogram (-1 = noise)
for i in np.arange(len(c)):
    if c[i]==-1:
        c[i] = c[i-1]
plt.plot(np.arange(len(c)),c)

#%% 

def av_rows(array):
    return  1. * np.sum(array.reshape(int(1 * array.shape[0] / 2),2, array.shape[1]),axis = 1) / array.shape[1]

def av_cols(array):
    pass

def moving_average(a, n=2) :
    ret = np.cumsum(a, dtype=float,axis=1)
    ret[:,n:] = ret[:,n:] - ret[:,:-n]
    return ret[:,n - 1:] / n

def rolling_window(a, window):
    a = np.concatenate((np.array([0]*(window-1)),a))
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

#%%

## smoothed hypnogram
    
c_smoothed = np.zeros_like(c)
window_size = 30
i=0
for window in rolling_window(c, window_size):
    c_smoothed[i] = np.argmax(np.bincount(window))
    i=i+1
    
plt.figure(figsize=(8,5))
plt.plot(np.arange(len(c_smoothed)),c_smoothed)

plt.title("Session ID-"+str(idx)+" "+sessionName+" RPCA="+str(rpca_components)+" PCA="+str(pca_components)+"+UMAP \n Possible Hypnogram. SMOOTHED (window="+str(window_size)+") \n labels may not be in the correct order")


#%%

samples1 = samples
samples1/=np.max(samples1)
samples1=(samples1+1)/2

fig = plt.figure(figsize=(400,8))
ax = fig.add_subplot(211)
#ax.set_aspect(aspect=0.5)
#plt.imshow(samples1[::10].T, cmap='hot', interpolation='nearest')

plt.imshow(samples1.T, cmap='seismic', interpolation='nearest')

plt.title("Session ID-"+str(idx)+" "+sessionName+" RPCA="+str(rpca_components)+" PCA="+str(pca_components)+"+UMAP \n Possible Hypnogram compare to spectogram", fontsize=36)

#ax = fig.add_subplot(312)
#plt.imshow(samples_ds.T, cmap='seismic', interpolation='nearest')

ax = fig.add_subplot(212)

# plot hypnogram (-1 = noise)
for i in np.arange(len(c)):
    if c[i]==-1:
        c[i] = c[i-1]
ax.plot(np.arange(len(samples)),c)
ax.set_xlim(0,len(samples))

plt.savefig("images/specto_vs_umapCluster"+str(idx)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+".png")

plt.show()

#%%

fig = plt.figure(figsize=(400,3))
ax = fig.add_subplot(111)
#plt.imshow(c_dup.T, cmap='seismic', interpolation='nearest')

sns.heatmap(c_dup.T,cmap=sns.color_palette("hls", len(np.unique(c))))


#%%

df = pd.DataFrame()
df['umap-2d-one'] = umap_results[np.where(c==0),0].flatten()
df['umap-2d-two'] = umap_results[np.where(c==0),1].flatten()
plt.figure(figsize=(16,10))
sns.scatterplot(
    x="umap-2d-one", y="umap-2d-two",
    data=df,
    legend="full",
    alpha=0.3
)


#%%

df = pd.DataFrame()
df['umap-2d-one'] = umap_results[:,0]
df['umap-2d-two'] = umap_results[:,1]
plt.figure(figsize=(16,10))
sns.scatterplot(
    x="umap-2d-one", y="umap-2d-two",
    hue = c,
    palette=sns.color_palette("hls", len(np.unique(c))),
    data=df,
    legend="full",
    alpha=0.3
)

for c1 in np.unique(c):
    x= umap_results[np.where(c==c1),0].mean()
    y= umap_results[np.where(c==c1),1].mean()
    plt.text(x,y,s=str(c1))
    
plt.title("Session ID-"+str(idx)+" "+sessionName+" RPCA="+str(rpca_components)+"+PCA="+str(pca_components)+"+UMAP \n clusters enamurated")
    
#%%
    
plt.figure(figsize=(16,10))

color_ts = np.arange(len(umap_results))

plt.scatter(umap_results[:,0], umap_results[:,1], alpha = .3, c = color_ts, cmap = 'copper')
cbar = plt.colorbar()

plt.title("Session ID-"+str(idx)+" "+sessionName+"\n color by time \n  RPCA="+str(rpca_components)+" PCA="+str(pca_components)+"+UMAP ")

plt.savefig("images/s"+str(idx)+"_RPCA="+str(rpca_components)+" _PCA="+str(pca_components)+"+UMAP_timecolor.png")
    
    
#%%

data=[]
lengths=[]
for c1 in np.unique(c):
    data.append(np.where(c==c1)[0])
    lengths.append(len(np.where(c==c1)[0]))
lengths = np.array(lengths)

fig = plt.figure(figsize=(16,10))
plt.boxplot(data,
            labels = np.arange(-1,len(np.unique(c))-1),
            widths = lengths/2000
            )
plt.title("Session ID-"+str(idx)+" "+sessionName+" RPCA="+str(rpca_components)+"+PCA="+str(pca_components)+"+UMAP \n clusters concentration (width related to number of points per cluster)")


#%%

# Try to add timestamp to PCA before UMAP

X_pca1 = np.concatenate((X_pca,np.expand_dims(timestamp,axis=1)),axis=1)

reducer  = umap.UMAP(n_neighbors=10, min_dist=0.1, metric="euclidean") #those are deafult params in the package
umap_results = reducer.fit_transform(X_pca1)

clf = DBSCAN(eps=0.4,min_samples=20)
c = clf.fit_predict(umap_results)

# plot 2-D decomposition with clustering result
df = pd.DataFrame()
df['umap-2d-one'] = umap_results[:,0]
df['umap-2d-two'] = umap_results[:,1]
plt.figure(figsize=(16,10))
sns.scatterplot(
    x="umap-2d-one", y="umap-2d-two",
    hue = c,
    palette=sns.color_palette("hls", len(np.unique(c))),
    data=df,
    legend="full",
    alpha=0.3
)
plt.title("Session ID-"+str(idx)+" "+sessionName+" RPCA="+str(rpca_components)+"+PCA="+str(pca_components)+"+UMAP ")
