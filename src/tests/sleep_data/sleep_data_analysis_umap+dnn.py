# -*- coding: utf-8 -*-
"""sleep_data_analysis_umap+dnn.ipynb
"""

import pickle
import mlflow
import mlflow.tensorflow

import sklearn
from sklearn.preprocessing import FunctionTransformer
from sklearn.metrics import mean_squared_error,adjusted_rand_score
from sklearn.model_selection import train_test_split

import tensorflow as tf
import numpy as np
import pandas as pd
from collections import namedtuple

import umap.umap_ as umap

import os
import sys
import glob
import matplotlib.pyplot as plt

sys.path.insert(0,os.path.join(os.getcwd(),"lib"))

import skRPCA
import ClusterFinder
import Vizualization
import experiment_utils as utils

viz = Vizualization.Vizualization()

df_labeled = utils.load_data('../data/df_features_test_env_labeled.pkl')
labeled_data = utils.load_data('../data/df_labels_test_env.pkl')

sessionNames_list = df_labeled.index.levels[0]
print(sessionNames_list)

#%%

def all_but_first_column(X):
    return X[:, 1:]

hyperparameter_defaults = dict(
  dropout = 0.0,
  lstm_size = 64,
  window_size = 15,
  epochs = 30,
  n_rpca_components = 121,
  n_pca_components = 10,
  umap_n_neighbors = 10,
  umap_min_dist = 0.1,
  umap_metric = 'euclidean',
  dbscan_eps = 0.4,
  dbscan_min_samples = 20
)

#wandb.init(project="ml2020_umap_dnn",config=hyperparameter_defaults,name="lstm64")
#config = wandb.config
config = namedtuple("myobj", hyperparameter_defaults.keys())(*hyperparameter_defaults.values())

pipe = sklearn.pipeline.Pipeline(steps=[
    # ('scalar', StandardScaler()),
    ('rpca', skRPCA.skRPCA(n_components=config.n_rpca_components)),
    ('pca', utils.PCA(n_components=config.n_pca_components)),
    ('remove_dimension', FunctionTransformer(all_but_first_column)),
    ('reduce', umap.UMAP(
        n_neighbors=config.umap_n_neighbors,
        min_dist=config.umap_min_dist, 
        metric=config.umap_metric)),
    ('model', utils.AgglomerativeClustering(n_clusters=4)),
])

window_size = config.window_size

with tf.device("/gpu:0"):

  x_data = np.empty((0, window_size, 122), int)
  y_data = np.empty((0, 1), int)
  cluster_data = np.empty((0, window_size,), int)
  
  sessionNames_list=[sessionNames_list[2]]

  for idx in range(len(sessionNames_list)):

      sessionName = sessionNames_list[idx]
      print(f"sessionName:{sessionName}", end =" ") 

      samples = utils.filter_by(df_labeled,{'sessionName' : [sessionName]})
      timestamp = samples.reset_index().timestamp
      samples = samples.get_values()
      samples = pd.DataFrame(data=samples,index=timestamp)

      print(f"data:{samples.shape}", end =" ") 
      

      labels = utils.filter_by(labeled_data,{'sessionName' : [sessionName]}).reset_index()
      hypno = labels.y
      hypno_ts = labels.timestamp
      hypno.index = list(labels.timestamp)

      hypno_val = hypno.replace(['U','W','R','N2','N3'],[0,1,2,3,4])
    
      print(f"hypno_val:{len(hypno_val)}")

      start = np.round((hypno_ts[0]-timestamp[0])/(window_size*1000)).astype(int)
      if (start>0):    
          samples=samples[start:]
          
      if (samples.shape[0]/window_size!=0):
          end = int(samples.shape[0]/window_size)*window_size
          samples=samples[:end]

      model = ClusterFinder.ClusterFinder(pipe)
      umap_results = model.special_fit_transforms(samples)
      clf = pipe['model']
      predicted = clf.fit_predict(umap_results)
      predicted=predicted+1 #change -1 to 0. 
          
      samples['ind'] = np.arange(samples.shape[0])/10000 #10K, just a big number
      samples = samples.to_numpy()

      samples = samples.reshape(int(samples.shape[0]/window_size),window_size,122)
      predicted = predicted.reshape(int(predicted.shape[0]/window_size),window_size,)
      
      if (samples.shape[0]>hypno_val.shape[0]):
          end = hypno_val.shape[0]
          samples=samples[:end]
          predicted=predicted[:end]

      

      cluster_data = np.vstack((cluster_data,predicted))
      x_data = np.vstack((x_data,samples))
      y_data = np.vstack((y_data,hypno_val.to_numpy().reshape(-1, 1)))
      
      print(f"s0:{samples.shape}|{predicted.shape}|{y_data.shape}")
      
      #del pipe
      #del model
      #del umap_results
      #del clf
      #del predicted      

  #%%

  n_classes = len([0,1,2,3,4])
  n_clusters = 30

  tf.compat.v1.keras.backend.clear_session()

  sample_inputs = tf.keras.layers.Input((window_size, 122))
  cluster_inputs = tf.keras.layers.Input((window_size,), dtype=tf.int32)

  embedding_layer = tf.keras.layers.Embedding(input_dim=n_clusters + 1,
                                              output_dim=25,
                                              input_length=15)
  embedded_clusters = embedding_layer(cluster_inputs)
  
  concated = tf.keras.layers.concatenate(
      [sample_inputs, embedded_clusters], axis=-1)

  #conv1 =  tf.keras.layers.Conv1D(16, kernel_size=8, padding='SAME')
  #res = conv1(inputs)

  res = tf.keras.layers.LSTM(config.lstm_size,dropout=config.dropout)(embedded_clusters)
  #res = tf.keras.layers.LSTM(config.lstm_size,dropout=config.dropout)(res)
  res = tf.keras.layers.Dense(config.lstm_size, activation=tf.nn.leaky_relu)(res)
  res = tf.keras.layers.Dense(n_classes, activation=tf.nn.softmax)(res)
  model = tf.keras.models.Model([sample_inputs, cluster_inputs], res)

  #x_train, x_test, y_train, y_test = train_test_split(x_data,
  #                                                    y_data,
  #                                                    test_size=0.15)

  split_point = int(len(x_data)*0.85)

  x_train = x_data[:split_point]
  x_test = x_data[split_point:]
  cluster_data_train = cluster_data[:split_point]
  cluster_data_test = cluster_data[split_point:]
  y_train = y_data[:split_point]
  y_test = y_data[split_point:]


  #mlflow.tensorflow.autolog()

  model.compile(optimizer='rmsprop',
                loss='sparse_categorical_crossentropy',
                metrics=['accuracy'])
  
  model.fit([x_train, cluster_data_train],
            y_train,
            validation_data=([x_test, cluster_data_test], y_test),
            batch_size=4,
            #callbacks=[LossAndErrorPrintingCallback()],
            callbacks=[WandbCallback()],
            epochs=config.epochs)

#mlflow.end_run()

clf.fit_predict(umap_results).shape

x_data[:int(len(x_data)*0.85)].shape,y_data.shape

infile = open('data/sleep_data/df_features_test_env_unlabeled.pkl','rb')
df_unlabeled = pickle.load(infile)
infile.close()

sessionNames_unlabeled_list = df_unlabeled.index.levels[0]
print(sessionNames_unlabeled_list)

for idx in range(len(sessionNames_unlabeled_list)):

  sessionName_test = sessionNames_unlabeled_list[idx]
  print(f"sessionName:{sessionName_test}", end =" ") 

  samples = utils.filter_by(df_unlabeled,{'sessionName' : [sessionName_test]})

  timestamp = samples.reset_index().timestamp
  samples = samples.get_values()
  samples = pd.DataFrame(data=samples,index=timestamp)

  if (samples.shape[0]/window_size!=0):
      end = int(samples.shape[0]/window_size)*window_size
      samples=samples[:end]

  samples['ind'] = np.arange(samples.shape[0])/10000 #10K, just a big number
  samples = samples.to_numpy()
  samples = samples.reshape(int(samples.shape[0]/window_size),window_size,122)

  print(f"s0:{samples.shape}")

  prediction = model.predict(samples)
  hypno = np.argmax(prediction, axis=1)
  hypno_val = pd.Series(data=hypno,index=np.arange(len(hypno)))
  hypno_val = hypno_val.replace([0,1,2,3,4],['U','W','R','N2','N3'])

  viz.plot_labeled_hypnogram(hypno_val, title=f"keras model LSTM32+conv1d_16x8 \n predict on unlabled session #{idx}:{sessionName_test}",fname=f"keras_lstm632_conv1d16x8_s{idx}")