"""
ClusterFinder.py: Main file for holding sklearn pipeline including helper methods

Expects a sklearn pipeline variable that can have any number of pre-processing layers.
The last layer should be a clustering sklearn model (named 'model' inside the pipeline) 
All layers of the pipeline should follow the sklearn api (should have fit,fit_transform,predict methods)
"""

# Author: Hezi Hershkovitz <hershkoy@gmail.com>
#         Guy Gurevich <guygu4@gmail.com>

import numpy as np
import scipy
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
import sklearn
from sklearn.preprocessing import FunctionTransformer
from random import randint
from sklearn.metrics import confusion_matrix
from sklearn.metrics import silhouette_score
from sklearn.base import clone
from scipy.spatial.distance import cdist
from kneed import KneeLocator
from kneebow.rotor import Rotor
from umap import umap_ as umap

from src.utils import experiment_utils as utils
from src.utils import skRPCA
from src.utils import skTSNE



class ClusterFinder:
    def getPipe(self):
        return self.pipe
    
    def getModelStr(self):
        return self.model_str
    
    def __init__(self, pipe=None, config=None):
        self.model_score=0
        self.k_val=0
        self.pipe = None
        self.model_str = ' '

        if pipe is not None:
            self.pipe = pipe
        elif config is not None:
            steps = []
            steps2dbg = []
            if (config['n_rpca_components']>0):
                n_rpca_components = config['n_rpca_components']
                steps.append(('rpca', skRPCA.skRPCA(n_components=n_rpca_components)))
                steps2dbg.append(f"rpca{n_rpca_components}")
            
            if (config['n_pca_components']>0):
                n_pca_components = config['n_pca_components']
                steps.append(('pca', PCA(n_components=n_pca_components)))
                steps2dbg.append(f"rpca{n_pca_components}")
            
            if (config['remove_first_dim']):
                steps.append(('remove_dimension', FunctionTransformer(utils.all_but_first_column)))
                steps2dbg.append(f"rmv1d")
                
            if 'tsne_perplexity' in config.keys():
                tsne_perplexity = config['tsne_perplexity']
                tsne_n_components = config['tsne_n_components']
                
                steps.append(('reduce', skTSNE.skTSNE(
                    n_components=tsne_n_components, 
                    verbose=1, 
                    perplexity=tsne_perplexity, 
                    n_iter=300)))
                steps2dbg.append(f"tsne({tsne_perplexity})")
                
            elif 'umap_min_dist' in config.keys():
                umap_params = {'verbose':True}
                for confkey in config.keys():
                    if confkey.startswith('umap_'):
                        umapkey = confkey.replace("umap_",'')
                        umap_params[umapkey] = config[confkey]
               
                steps.append(('reduce', umap.UMAP(**umap_params)))
                steps2dbg.append(f"umap({config['umap_n_neighbors']},{config['umap_min_dist']})")
            else:
                raise ValueError("ClusterFinder expect umap or tsne, none was given")
            
            dbscan_eps = config['dbscan_eps']
            dbscan_min_samples = config['dbscan_min_samples']
            steps.append(('model', utils.DBSCAN(eps=dbscan_eps, min_samples=dbscan_min_samples)))
            steps2dbg.append(f"dbscan({dbscan_eps},{dbscan_min_samples})")
            
            self.pipe = Pipeline(steps=steps)
            self.model_str = ' '.join(steps2dbg)
            print(f"model_str:{self.model_str}")

        else:
            raise ValueError("ClusterFinder expect pipe or config, none was given")
    
    
    def fit(self, X,y=[]):
        """apply fix on the pipeline
        X: the actual data 
        y: (optional) the labels
        """           
        if len(y)==0:
            self.pipe.fit(X)
        else:
            self.pipe.fit(X,y)
            
    def just_transforms(self, X,max_depth=-1):
        """Applies all transforms to the data, without applying last 
           estimator.
    
        Parameters
        ----------
        X : iterable
            Data to predict on. Must fulfill input requirements of first step of
            the pipeline.
        """
        Xt = X
        for name, transform in self.pipe.steps[:max_depth]:
            Xt = transform.transform(Xt)
        return Xt            

    def special_fit_transforms(self, X,y=None,max_depth=-1):
        """Applies all fit_transforms to the data, without applying last 
           estimator.
    
        Parameters
        ----------
        X : iterable
            Data to predict on. Must fulfill input requirements of first step of
            the pipeline.
        """        
        Xt = X
        for _, step in self.pipe.steps[:max_depth]:
            Xt = step.fit_transform(Xt,y)
        return Xt   
    
    def predict(self, X):
        return self.pipe.predict(X)
    
    

    #BIC - Bayesian information criterion
    def optimal_k_bic(self,X,num_iterations=20,max_k=10,plot=False,verbose=False):
        """algorithm to calculate the optimal k of the model (for clustering methods that have it as an hyper parameter, like GMM)
        using BIC metric to optimize
        
        X: the actual data 
        num_iterations: number of iterations to test
        max_k: the maximum k
        plot: generate plot to show the result
        verbose: output extra info for better debugging
        
        based on:
        https://towardsdatascience.com/gaussian-mixture-model-clusterization-how-to-select-the-number-of-components-clusters-553bef45f6e4
        """  

        K = range(1,max_k)
        bic = np.zeros((len(K),num_iterations))
        for k in K:
            for i in range(num_iterations):
                self.pipe['model'].n_components=k
                X_pca = self.just_transforms(X)
                self.pipe['model'].fit(X_pca)
                bic_score = self.pipe['model'].bic(X_pca)
                bic[k-1,i] = bic_score
                if (verbose):
                    print("K#{}:{}, bic_score:{} ".format(k,i,bic_score))
        
        bic=np.mean(bic,axis=1)
        
        if (plot):
            plt.figure()
            plt.plot(K,bic)
            plt.title("BIC")
            print("bic:",bic)

        
        # find elbow: first +1 because k values are 1..10, not 0..9. second +1 is because we round up
        #self.k_val = np.argmax(np.roll(bic,1)-bic)+1+1   
            
        #kn = KneeLocator(K,bic, curve='convex', direction='decreasing')
        
        #rotor = Rotor()
        #rotor.fit_rotate(np.stack((K,np.flip(bic)),axis=-1))
        #rotor.plot_elbow()
        #self.k_val = rotor.get_elbow_index()
            
        self.k_val = np.argmin(bic)+1 #because K starts from 1.


        return self.k_val
    
    
    def optimal_k_silhouette(self,X,num_iterations=20,plot=False,verbose=False):    
        """algorithm to calculate the optimal k of the model (for clustering methods that have it as an hyper parameter, like GMM)
        using silhouette metric to optimise
        
        Since we already know that the fitting procedure is not deterministic, we run the fit several times for each number of clusters.
        silhouette seems to be very bad in presence of noise
        
        X: the actual data 
        num_iterations: number of iterations to test
        plot: generate plot to show the result
        verbose: output extra info for better debugging        
               
        based on:
        https://towardsdatascience.com/gaussian-mixture-model-clusterization-how-to-select-the-number-of-components-clusters-553bef45f6e4
        """  
        
        K = range(2,10)
        silhouette = np.zeros((len(K),num_iterations))
        for k in K:
            for i in range(num_iterations):
                self.pipe['model'].n_components=k
                X_pca = self.just_transforms(X)
                gmm = self.pipe['model']
                gmm.fit(X_pca)
                score = silhouette_score(X_pca, gmm.predict(X_pca))
                silhouette[k-2,i] = score
                if (verbose):
                    print("K#{}:{}, silhouette_score:{} ".format(k,i,score))
        
        """best five values, then average"""        
        silhouette=np.sort(silhouette,axis=1)[:,-5:]
        silhouette=np.mean(silhouette,axis=1)
        if (plot):
            plt.figure()
            plt.plot(K,silhouette)
            plt.title("silhouette")
            print("silhouette:",silhouette)

        self.k_val = np.argmax(silhouette)+2 #because K starts from 2.
        return self.k_val
    
    
    # taken from: https://github.com/arvkevi/kneed/blob/master/notebooks/decreasing_function_walkthrough.ipynb
    
    def optimal_k_distortions(self,X,plot=False,verbose=False):
        """algorithm to calculate the optimal k of the model (for clustering methods that have it as an hyper parameter, like GMM)
        using distortions score to optimise
        
        X: the actual data 
        plot: generate plot to show the result
        verbose: output extra info for better debugging        
               
        based on:
        https://towardsdatascience.com/gaussian-mixture-model-clusterization-how-to-select-the-number-of-components-clusters-553bef45f6e4
        """         

        distortions = []
        K = range(1,10)
        for k in K:
            self.pipe['model'].n_components=k
            X_pca = self.just_transforms(X)
            gmm = self.pipe['model']
            gmm.fit(X_pca)
            score = sum(np.min(cdist(X_pca, self.pipe['model'].means_, 'euclidean'), axis=1)) / X_pca.shape[0]
            distortions.append(score)
            
            if (verbose):
                print("K#{}: silhouette_score:{} ".format(k,score))
        
        if (plot):
            plt.figure()
            plt.plot(K,distortions)
            plt.title("distortions")
            print("distortions:",distortions)

        kn = KneeLocator(list(K),distortions, curve='convex', direction='decreasing')
        self.k_val = kn.knee

        return self.k_val 
