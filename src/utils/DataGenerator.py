"""
DataGenerator: Main file for generating blobs for simulated data
"""

# Author: Hezi Hershkovitz <hershkoy@gmail.com>
#         Guy Gurevich <guygu4@gmail.com>

import numpy as np
import scipy
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs,make_moons
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from random import randint
from sklearn.metrics import confusion_matrix
from sklearn.metrics import silhouette_score
from sklearn.base import clone
from scipy.spatial.distance import cdist
from kneed import KneeLocator
from kneebow.rotor import Rotor

class DataGenerator:
    from sklearn.datasets import make_blobs, make_moons
    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    from mpl_toolkits.mplot3d import Axes3D

    
    def __init__(self,method='blobs'):
        self.method=method
        
    def circle_points(self,r, n, n_features=2):
        circles = []
        two_pi = 2*np.pi
        t = np.linspace(0, two_pi, n+1)
        for t1 in t:
            x = r * np.cos(t1)
            y = r * np.sin(t1)
            if n_features==2:
                circles.append([x, y])
            else:
                z=np.random.uniform(low=-r,high=r,size=1)[0]
                circles.append([x, y, z])
                
        return circles[:-1]
    
    def generate(self,n_samples=100, n_features=3, cluster_std=0.5, minimum_distance=None, centers=None,random_state=None,cov=None):
        """
        Parameters
        ----------
        n_samples : int or array-like, optional (default=100)
            If int, it is the total number of points equally divided among
            clusters.
            If array-like, each element of the sequence indicates
            the number of samples per cluster.
        n_features : int, optional (default=3)
            The number of features for each sample.
        centers : int or array of shape [n_centers, n_features], optional
            (default=None)
            The number of centers to generate, or the fixed center locations.
            If n_samples is an int and centers is None, 3 centers are generated.
            If n_samples is array-like, centers must be
            either None or an array of length equal to the length of n_samples.
        cluster_std : float or sequence of floats, optional (default=1.0)
            The standard deviation of the clusters.
        random_state : int, or None (default)
            Determines random number generation for dataset creation
        cov: covariance matrix (to generate non-symmetrical data)
        """            
        
        #cluster_std = cluster_std[0]

        if (minimum_distance is not None):
            do_loop=True
            attempt=0
            
            while do_loop:
                attempt=attempt+1
                print(cluster_std,minimum_distance)
                centers_locations = np.random.rand(centers,n_features)*(10.0-cluster_std*3.0) #distance the centers from the edges
                distances = cdist(centers_locations,centers_locations)
                min_distance = np.min(distances[distances>0])
                print(distances,min_distance)
                do_loop =min_distance<minimum_distance
                if (do_loop and attempt>10):
                    print("minimum distance condition wasn't met within 10 attempts. abort.")
                    break
                
        elif (isinstance(centers, int)):
           centers = self.circle_points(r=5,n=centers,n_features=n_features)
           print("centers:",centers)
        
        
        if self.method=='blobs':   
            data = make_blobs(random_state=random_state,
                n_samples=n_samples, 
                centers=centers, 
                n_features=n_features,
                cluster_std=cluster_std)
        elif self.method=='moons':
            data = make_moons(n_samples=n_samples, shuffle=True, noise=None, random_state=random_state)
       
        if (cov is not None):
                data=(data[0] @ cov,data[1])
                
        return data

    def add_noise(self,X,noise_channels_to_add,noise_level=1):
        """
        Parameters
        ----------
        X : the data
        noise_channels_to_add : int, number of channels of noise to add to the data
        noise_level : float, optional. amount of noise to add (SNR)
        """            
        noise_channels = np.zeros((len(X),noise_channels_to_add))
        X= np.hstack((X,noise_channels))
        
        noise = np.random.uniform(
                low=noise_level*np.max(X), 
                high=noise_level*np.min(X),
                size=X.shape)
        X = X+noise
        return X
    
    def draw_data(self, X, y, title):
        df = pd.DataFrame(X)
        fig = plt.figure()
        if (np.shape(df)[1]==1):
            ax = fig.add_subplot()
            ax.hist(df[0])
            ax.set_title(title)
        elif (np.shape(df)[1]==2):
            ax = fig.add_subplot()
            ax.scatter(df[0],df[1],c=y,edgecolor='k',s=20)
            ax.set_title(title)
        elif (np.shape(df)[1]==3):
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(df[0],df[1],df[2],c=y,edgecolor='k',s=20)
            ax.set_title(title)
        elif (np.shape(df)[1]>3):
            lst_vars=list(combinations(df.columns,2))
            plt.figure(figsize=(20,20))
            for i in range(1,len(lst_vars)+1):
                plt.subplot(10,10,i)
                dim1=lst_vars[i-1][0]
                dim2=lst_vars[i-1][1]
                plt.scatter(df[dim1],df[dim2],c=y,edgecolor='k',s=20)
                plt.xlabel(f"{dim1}",fontsize=10)
                plt.ylabel(f"{dim2}",fontsize=10)
                plt.xlim((-10,10))
                plt.ylim((-10,10))
                plt.title(title)