from sklearn.metrics import confusion_matrix

def clustered_label_accuracy_score(y,y_pred):
    mappings_array = confusion_matrix(y, y_pred)  
    mapping_dict={}
    correct = np.zeros(len(y))
    while np.max(mappings_array)>0:
        location = np.where(mappings_array==np.max(mappings_array))
        orig_label = location[0][0]
        predicted_label = location[1][0]
        mapping_dict[orig_label]=predicted_label
        mappings_array[orig_label,:]=0
        mappings_array[:,predicted_label]=0
        tmp = np.where(y==orig_label)[0]
        correct[tmp[y_pred[np.where(y==orig_label)]==predicted_label]]=1
    print(mapping_dict)
    return {'accuracy':np.sum(correct)/len(y),'check':np.vstack((y,y_pred,correct)).T}