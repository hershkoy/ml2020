"""
Vizualization: helper visualization methods commonly used in our tests files
"""

# Author: Hezi Hershkovitz <hershkoy@gmail.com>
#         Guy Gurevich <guygu4@gmail.com>


import numpy as np
import matplotlib.pyplot as plt

import os
import matplotlib.cm as cmx
import matplotlib.patches as patches
from matplotlib.colors import LogNorm
import matplotlib.ticker
import plotly.express as px
import pandas as pd
import seaborn as sns
import plotly
import plotly.express as px
import plotly.graph_objects as go
import plotly.offline as py


running_env ="spyder" # "colab"

class Vizualization:

    def visualize_3d_gmm(self, points, w, mu, stdev, export=False, newplot=True):
        '''Plots points and their corresponding gmm model in 3D
        Input: 
            points: N X 3, sampled points
            w: n_gaussians, gmm weights
            mu: 3 X n_gaussians, gmm means
            stdev: 3 X n_gaussians, gmm standard deviation (assuming diagonal covariance matrix)
        Output:
            None
            
        adapted from: http://www.itzikbs.com/gaussian-mixture-model-gmm-3d-point-cloud-classification-primer
        '''
    
        n_gaussians = mu.shape[1]
        N = int(np.round(points.shape[0] / n_gaussians))
        # Visualize data
        if(newplot):
            fig = plt.figure(figsize=(8, 8))
        axes = fig.add_subplot(111, projection='3d')
        #axes.set_xlim([-1, 1])
        #axes.set_ylim([-1, 1])
        #axes.set_zlim([-1, 1])
        plt.set_cmap('Set1')
        colors = cmx.Set1(np.linspace(0, 1, n_gaussians))
        for i in range(n_gaussians):
            idx = range(i * N, (i + 1) * N)
            axes.scatter(points[idx, 0], points[idx, 1], points[idx, 2], alpha=0.3, c=colors[i])
            plot_sphere(w=w[i], c=mu[:, i], r=stdev[:, i], ax=axes)
    
        plt.title('3D GMM')
        axes.set_xlabel('X')
        axes.set_ylabel('Y')
        axes.set_zlabel('Z')
        axes.view_init(35.246, 45)
        if export:
            if not os.path.exists('images/'): os.mkdir('images/')
            plt.savefig('images/3D_GMM_demonstration.png', dpi=100, format='png')
        plt.show()
    
    def plot_sphere(self,w=0, c=[0,0,0], r=[1, 1, 1], subdev=10, ax=None, sigma_multiplier=3, newplot=True):
        '''Plot a sphere surface.
        Input: 
            c: 3 elements list, sphere center
            r: 3 element list, sphere original scale in each axis ( allowing to draw elipsoids)
            subdiv: scalar, number of subdivisions (subdivision^2 points sampled on the surface)
            ax: optional pyplot axis object to plot the sphere in.
            sigma_multiplier: sphere additional scale (choosing an std value when plotting gaussians)
        Output:
            ax: pyplot axis object
        '''
    
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
        pi = np.pi
        cos = np.cos
        sin = np.sin
        phi, theta = np.mgrid[0.0:pi:complex(0,subdev), 0.0:2.0 * pi:complex(0,subdev)]
        x = sigma_multiplier*r[0] * sin(phi) * cos(theta) + c[0]
        y = sigma_multiplier*r[1] * sin(phi) * sin(theta) + c[1]
        z = sigma_multiplier*r[2] * cos(phi) + c[2]
        cmap = cmx.ScalarMappable()
        cmap.set_cmap('jet')
        c = cmap.to_rgba(w)
    
        ax.plot_surface(x, y, z, color=c, alpha=0.2, linewidth=1)
    
        return ax
    
    def visualize_2d_gmm(self,points, w, mu, stdev, export=False, newplot=True):
        """Plots points and their corresponding gmm model in 2D.
        Input: 
            points: N X 2, sampled points
            w: n_gaussians, gmm weights
            mu: 2 X n_gaussians, gmm means
            stdev: 2 X n_gaussians, gmm standard deviation (assuming diagonal covariance matrix)
        Output:
            None
        """
        n_gaussians = mu.shape[1]
        N = int(np.round(points.shape[0] / n_gaussians))
        # Visualize data
        if (newplot):
            plt.figure(figsize=(8, 8))
        axes = plt.gca()
        #axes.set_xlim([-1, 1])
        #axes.set_ylim([-1, 1])
        plt.set_cmap('Set1')
        colors = cmx.Set1(np.linspace(0, 1, n_gaussians))
        for i in range(n_gaussians):
            idx = range(i * N, (i + 1) * N)
            plt.scatter(points[idx, 0], points[idx, 1], alpha=0.3, c=colors[i])
            for j in range(8):
                axes.add_patch(
                    patches.Ellipse(mu[:, i], width=(j+1) * stdev[0, i], height=(j+1) *  stdev[1, i], fill=False, color=[0.0, 0.0, 1.0, 1.0/(0.5*j+1)]))
            plt.title('GMM')
        plt.xlabel('X')
        plt.ylabel('Y')
    
        if export:
            if not os.path.exists('images/'): os.mkdir('images/')
            plt.savefig('images/2D_GMM_demonstration.png', dpi=100, format='png')
    
        plt.show()
        
    def visualize_gmm(self,gmm,X, newplot=True):
        """Plots gmm result automatically (2D or 3D)
        Input: 
            X: results from gmm predict 
            gmm: the gmm model
            newplot: create a new plot figure (if false, use current).
        Output:
            None
        """        
        
        if X.shape[1] == 3:
            self.visualize_3d_gmm(X, gmm.weights_, gmm.means_.T, np.sqrt(gmm.covariances_).T)
        elif X.shape[1] == 2 and gmm.covariance_type == 'diag':
            self.visualize_2d_gmm(X, gmm.weights_, gmm.means_.T, np.sqrt(gmm.covariances_).T)    
        elif X.shape[1] == 2:
            if (newplot):
                plt.figure()
            contour_axes = np.linspace(np.min(X), np.max(X))
            mesh_x1,mesh_x2 = np.meshgrid(contour_axes, contour_axes)
            mesh = np.array([mesh_x1.ravel(), mesh_x2.ravel()]).T
            Z = -gmm.score_samples(mesh)
            Z = Z.reshape(mesh_x1.shape)
            plt.contour(mesh_x1, mesh_x2, Z, 
                        norm=LogNorm(vmin=1.0, vmax=1000.0),
                        levels=np.logspace(0, 3, 10))
            plt.scatter(X[:, 0], X[:, 1], .8)
            plt.scatter(gmm.means_[:,0],gmm.means_[:,1])
    
    def make_chart (self,X, y_pred, newplot=True,size=None,title='',ax=None):
        """automatically plot a 2D or 3D chart with predictions
        Input: 
            X: inputs
            y_pred: predictions. should be same length as input.
            newplot: create a new plot figure (if false, use current).
            size:xlim & ylimof the plot
            title:the figure title
        Output:
            None
        """          
        if X.shape[1] == 2:
            self.make_2d_chart (X, y_pred, newplot=newplot,size=size,title=title,ax=ax)
            return
        if newplot:
          fig = plt.figure()
        if (running_env=="spyder"):
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(X[:,0], X[:,1], X[:,2], c=y_pred)
        else:
            fig=px.scatter_3d(x=X[:,0], y=X[:,1], z=X[:,2], color=y_pred)
            fig.show()
        ax.title.set_text(title)
        
    def make_2d_chart (self,X, y_pred, newplot=True,size=None,title='',ax=None):
        """helper function to plot a 2D chart with predictions
        Input: 
            X: inputs should be 2D
            y_pred: predictions. should be same length as input.
            newplot: create a new plot figure (if false, use current).
            size:xlim & ylimof the plot
            title:the figure title
        Output:
            None
        """          
        if newplot:
          fig = plt.figure()
          plt.scatter(X[:,0], X[:,1], c=y_pred)  
          if (size is not None):
                plt.xlim(size[0],size[1])
                plt.ylim(size[0],size[1])
          plt.title(title)
          
        if ax is not None:
            plt_o = ax
            plt_o.scatter(X[:,0], X[:,1], c=y_pred)  
            if (size is not None):
                plt_o.set_xlim(size[0],size[1])
                plt_o.set_ylim(size[0],size[1])
            plt_o.set_title(title)
       
        
    
    def pca_visualization(self,X,y,title='', newplot=True):
        """helper visualization function to plot a PCA results (2D or 3D)
        Input: 
            X: the inputs (can be 2D or 3D)
            y: predictions. should be same length as input.
            newplot: create a new plot figure (if false, use current).
            title:the figure title
        Output:
            None
        """      
        
        if X.shape[1] == 3:
            make_chart(X, y,newplot=newplot)
        elif X.shape[1] == 2:
            if newplot:
                plt.figure()
            if y is None:
                scatter  = plt.scatter(X[:,0], X[:,1])
            else:
                scatter  = plt.scatter(X[:,0], X[:,1], c=y)
                classes = list(set(y))
                plt.legend(handles=scatter.legend_elements()[0], labels=classes)
            plt.title(title)

    def plot_umap(self, umap_results, pred=None, enumerated=False, title=None, fname=None,ax=None):
        """helper function to plot umap results
        Input: 
            umap_results: the results from the umap
            pred: predictions (integers, optional) should be same length as input.
            enumerated: (optional) if true, enumurate the clusters
            title: (optional) the figure title
            fname: (optional) name of file to save the chart
            ax: (optional) plot the chart into ax object. useful for subplots
        Output:
            None
        """     
        # plot 2-D decomposition with clustering result
        df = pd.DataFrame()
        df['umap-2d-one'] = umap_results[:,0]
        df['umap-2d-two'] = umap_results[:,1]
        
        if (ax is None):
            plt_o = plt
            plt_o.figure(figsize=(10,6))
        else:
            plt_o = ax
        
        sns.scatterplot(
            x="umap-2d-one",
            y="umap-2d-two",
            hue=pred,
            palette=sns.color_palette("hls", len(np.unique(pred))),
            data=df,
            legend="full",
            alpha=0.3
        )
        
        if enumerated:
            for c1 in np.unique(pred):
                x= umap_results[np.where(pred==c1),0].mean()
                y= umap_results[np.where(pred==c1),1].mean()
                plt_o.text(x,y,s=str(c1))

        if title is not None:
            if (ax is None):
                plt_o.title(title)
            else:
                plt_o.set_title(title)
            
        if fname is not None:
            plt.savefig(fname)

    def plotly_scatter_3d(self, points, labels=None,  htmlfname=None):
        """helper function to plot umap results
        Input: 
            points: the 3d data
            labels: (integers, optional) should be same length as points input.
            htmlfname: (optional) name of html file to save plotly chart
        Output:
            None
        """     
        df = pd.DataFrame()

        if labels is None:
            labels = np.ones(len(points))

        df['one'] = points[:,0][:len(labels)]
        df['two'] = points[:,1][:len(labels)]
        df['three'] = points[:,2][:len(labels)]
        df['labels'] = list(labels)

        #fig = px.scatter_3d(df,x='one',y='two',z='three', 
        #                    color='labels',
        #                    mode='markers',
        #                    marker=dict(size=4, symbol="circle", color=df["labels"]),
        #                    width=1200, height=1200)

        fig = go.FigureWidget(data=[go.Scatter3d(
            x=df['one'],y=df['two'],z=df['three'],
            mode='markers',
            marker=dict(
                size=2,
                color=df['labels'], # set color to an array/list of desired values
                opacity=0.8
            )
            )])

        fig.update_layout(margin=dict(l=0, r=0, b=0, t=0))

        py.iplot(fig)
        #fig.show(renderer="colab")
            
        if htmlfname is not None:
            #plt.savefig(htmlfname)
            fig.write_html(htmlfname+".html")


    def plot_moving_averages(self, data, pred, title=None, fname=None):
        def av_rows(array):
            return  1. * np.sum(array.reshape(int(1 * array.shape[0] / 2), 2, array.shape[1]),axis = 1) / array.shape[1]

        def av_cols(array):
            pass

        def moving_average(a, n=2) :
            ret = np.cumsum(a, dtype=float,axis=1)
            ret[:,n:] = ret[:,n:] - ret[:,:-n]
            return ret[:,n - 1:] / n

        samples1 = data
        samples1 /= np.max(samples1)
        samples1 = (samples1 + 1)/2

        fig = plt.figure(figsize=(400,8))
        ax = fig.add_subplot(211)


        plt.imshow(samples1.T, cmap='seismic', interpolation='nearest')
        if title is not None:
            plt.title(title, fontsize=36)

        ax = fig.add_subplot(212)

        # plot hypnogram (-1 = noise)
        for i in np.arange(len(pred)):
            if pred[i]==-1:
                pred[i] = pred[i-1]
        ax.plot(np.arange(len(data)), pred)
        ax.set_xlim(0,len(data))
        if fname is not None:
            plt.savefig(fname)
        plt.show()

    def plot_heatmap(self, umap_results, pred, title=None, fname=None):
        """plot the neurosteer data as heatmap on the time axis
        Input: 
            umap_results: ???
            pred: the actual data
            title: (optional) the figure title
            fname: (optional) name of file to save the chart
        Output:
            None
        """   
        
        fig = plt.figure(figsize=(400,3))
        ax = fig.add_subplot(111)
        #plt.imshow(c_dup.T, cmap='seismic', interpolation='nearest')
        print(pred.shape, pred.T.shape)
        sns.heatmap(pred.T, cmap=sns.color_palette("hls", len(np.unique(pred))))
        if title is not None:
            plt.title(title)
        if fname is not None:
            plt.savefig(fname)

    def plot_timecolor(self, umap_results, pred, title=None, fname=None):
        """plot the umap coloring samples according to time 
        Input: 
            umap_results: the umap results 
            pred: predictions (integers, optional) should be same length as input.
            title: (optional) the figure title
            fname: (optional) name of file to save the chart
        Output:
            None
        """           
        plt.figure(figsize=(16,10))

        color_ts = np.arange(len(umap_results))

        plt.scatter(umap_results[:,0], umap_results[:,1], alpha = .3, c = color_ts, cmap = 'copper')
        cbar = plt.colorbar()
        if title is not None:
            plt.title(title)
        if fname is not None:
            plt.savefig(fname)
    
    def rolling_window(self,a, window):
        """rolling window on a 1D signal 
        Input: 
            a: the input data 
            window: the amount of window to average the input by 
        Output:
            an list of rolling windows on each item of the original data.
        """          
        a = np.concatenate((np.array([0]*(window-1)),a))
        shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
        strides = a.strides + (a.strides[-1],)
        return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    
        
    def plot_hypnogram(self,c, smooth=None, title=None, fname=None,ax=None):
        """Plot hypnogram 
        Input: 
            c: the actual data (1d numpy array)
            smooth: amount of rolling window to apply on input.
            title: (optional) the figure title
            fname: (optional) name of file to save the chart
            ax: (optional) plot the chart into ax object. useful for subplots
        Output:
            None
        """           
        
        # plot hypnogram (-1 = noise)
        for i in np.arange(len(c)):
            if c[i]==-1:
                c[i] = c[i-1]
                
        if (smooth is not None):        
            c_smoothed = np.zeros_like(c)
            window_size = smooth
            i=0
            for window in self.rolling_window(c, window_size):
                hist,edges = np.histogram(window)
                most_freq = np.round(edges[np.argmax(hist)]).astype(int)
                c_smoothed[i] = most_freq
                i=i+1
            c=c_smoothed  
                            
        if (ax is None):
            plt_o = plt
            plt_o.figure(figsize=(10,6))
        else:
            plt_o = ax

        plt_o.plot(range(len(c)),c)
        if title is not None:
            if (ax is None):
                plt_o.title(title)
            else:
                plt_o.set_title(title)            
            
        if fname is not None:
            plt.savefig(fname)
            
        return c
    
    def plot_labeled_hypnogram(self,hypno, title=None, fname=None,ax=None):
        """Plot labeled hypnogram 
        Input: 
            hypno: the actual data (panda series). expected values are U,W,R,N2,N3
            title: (optional) the figure title
            fname: (optional) name of file to save the chart
            ax: (optional) plot the chart into ax object. useful for subplots
        Output:
            None
        """          
        
        hypno_val = hypno.replace(['U','W','R','N2','N3'],[-1,0,1,2,3])
        

        if (ax is None):
            plt_o = plt
            plt_o.figure(figsize=(10,6))
        else:
            plt_o = ax
            
        plt_o.plot(hypno_val)
        
        ylabels = ['U', 'W', 'R', 'N2', 'N3']
        
        if (ax is None):
            plt_o.yticks([-1, 0, 1, 2, 3], ylabels)
        else:
            plt_o.set_yticks([-1, 0, 1, 2, 3])
            plt_o.set_yticklabels(ylabels)
        
        if title is not None:
            if (ax is None):
                plt_o.title(title)
            else:
                plt_o.set_title(title)      
            
        if fname is not None:
            plt.savefig(fname)            
            
    def clusters_concentration(self,c, title=None, fname=None):
        """exploratory analysis on umap results to show cluster concentration over time using boxplots 
        Input: 
            c: the umap result 
            title: (optional) the figure title
            fname: (optional) name of file to save the chart
        Output:
            None
        """       
        data=[]
        lengths=[]
        for c1 in np.unique(c):
            data.append(np.where(c==c1)[0])
            lengths.append(len(np.where(c==c1)[0]))
        lengths = np.array(lengths)
        
        fig = plt.figure(figsize=(16,10))
        plt.boxplot(data,
                    labels = np.arange(-1,len(np.unique(c))-1),
                    widths = lengths/2000)
        
        if title is not None:
            plt.title(title)
        if fname is not None:
            plt.savefig(fname)
            


class FFormatter(matplotlib.ticker.ScalarFormatter):
    def __init__(self, fformat="%1.1f", offset=True, mathText=True):
        self.fformat = fformat
        matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,useMathText=mathText)
    def _set_format(self, vmin, vmax):
        self.format = self.fformat
        if self._useMathText:
            self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)