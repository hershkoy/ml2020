"""
experiment_utils: file for helper functions required during tests
"""


# Author: Hezi Hershkovitz <hershkoy@gmail.com>
#         Guy Gurevich <guygu4@gmail.com>

import pickle
from random import randint
import os

import numpy as np
import scipy
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
#import umap.umap_ as umap

from umap import umap_ as umap
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.mixture import GaussianMixture
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sklearn.metrics import silhouette_score
from sklearn.base import clone
from scipy.spatial.distance import cdist
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.cluster import DBSCAN,AgglomerativeClustering
from kneed import KneeLocator
from kneebow.rotor import Rotor
from sklearn.metrics import mean_squared_error,adjusted_rand_score

from src.utils import experiment_utils as utils


def load_data(pickle_file_path):
    infile = open(pickle_file_path,'rb')
    obj = pickle.load(infile)
    infile.close()
    return obj

def filter_by(df, constraints):
    """Filter MultiIndex by sublevels."""
    indexer = [constraints[name] if name in constraints else slice(None)
               for name in df.index.names]
    return df.loc[tuple(indexer)] if len(df.shape) == 1 else df.loc[tuple(indexer),]


def get_labeled_session(df_labeled,labeled_data,sessionName):
    samples = utils.filter_by(df_labeled,{'sessionName' : [sessionName]})
    timestamp = samples.reset_index().timestamp
    samples = samples.get_values()
    samples = pd.DataFrame(data=samples,index=timestamp)

    labels = utils.filter_by(labeled_data,{'sessionName' : [sessionName]}).reset_index()
    hypno = labels.y
    hypno_ts = labels.timestamp
    hypno.index = list(labels.timestamp)
    #hypno_val = hypno.replace(['U','W','R','N2','N3'],[0,1,2,3,4])

    return samples,timestamp,hypno


def adjust_hypno(orig,predicted,predicted_timestamp):
    """
    orig: timestamped series of labeled hypno
    """
    a = pd.Series(data=predicted,index=predicted_timestamp)
    
    delta_orig = np.mean(orig.index[1:] - orig.index[:-1])
    delta_predict = np.mean(a.index[1:] - a.index[:-1])
    
    downsmp = np.round(delta_orig/delta_predict).astype(int)
    
    a=a[::downsmp]
    
    start = np.round((orig.index[0]-a.index[0])/delta_orig).astype(int)
    if (start>0):    
        a=a[start:]
    
    if (len(a)>len(orig)):
        a=a[:len(orig)]
    
    return a

def all_but_first_column(X):
    return X[:, 1:]    
    
    
    

class Buffer:
    def __init__(self):
        self.buff = []
    def write(self, item):
        self.buff.append(item)



