"""
skRPCA: Wrapper file to allow adding RPCA into sklearn pipeline 
"""

# Author: Hezi Hershkovitz <hershkoy@gmail.com>
#         Guy Gurevich <guygu4@gmail.com>

from __future__ import division, print_function

import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator
import matplotlib.pyplot as plt


class skRPCA(TransformerMixin, BaseEstimator):
    
    def __init__(self, n_components=None,max_iter=1000, iter_print=100):
        self.max_iter=max_iter
        self.iter_print=iter_print
        self.n_components=n_components
        
    @staticmethod
    def frobenius_norm(M):
        return np.linalg.norm(M, ord='fro')

    @staticmethod
    def shrink(M, tau):
        return np.sign(M) * np.maximum((np.abs(M) - tau), np.zeros(M.shape))

    def svd_threshold(self, M, tau):
        U, S, V = np.linalg.svd(M, full_matrices=False)
        return np.dot(U, np.dot(np.diag(self.shrink(S, tau)), V))

    def fit(self, D):
        
        self.mu = np.prod(D.shape) / (4 * self.frobenius_norm(D))
        self.mu_inv = 1 / self.mu
        self.lmbda = 1 / np.sqrt(np.max(D.shape))
        iter = 0
        err = np.Inf
        Sk = np.zeros(D.shape)
        Yk = np.zeros(D.shape)
        Lk = np.zeros(D.shape)

        _tol = 1E-7 * self.frobenius_norm(D)

        while (err > _tol) and iter < self.max_iter:
            Lk = self.svd_threshold(
                D - Sk + self.mu_inv * Yk, self.mu_inv)
            Sk = self.shrink(
                D - Lk + (self.mu_inv * Yk), self.mu_inv * self.lmbda)
            Yk = Yk + self.mu * (D - Lk - Sk)
            err = self.frobenius_norm(D - Lk - Sk)
            iter += 1
            if (iter % self.iter_print) == 0 or iter == 1 or iter > self.max_iter or err <= _tol:
                print('iteration: {0}, error: {1}'.format(iter, err))
          
        self.L = Lk
        self.S = Sk
        self.D = D
                        
        return self
    
    def fit_transform(self, D, y=None):
        self.fit(D)
        Lk = self.L
        m, n = Lk.shape
        Lk -= Lk.mean(axis=0)
        R = np.cov(Lk, rowvar=False)
        # use 'eigh' rather than 'eig' since R is symmetric,
        # the performance gain is substantial
        evals, evecs = np.linalg.eigh(R)
        idx = np.argsort(evals)[::-1]
        self.evecs = evecs[:,idx]
        self.evals = evals[idx]
        if self.n_components is not None:
            self.evecs = evecs[:, :self.n_components]
        # carry out the transformation on the data using eigenvectors
        # and return the re-scaled data, eigenvalues, and eigenvectors
        return np.dot(self.evecs.T, Lk.T).T
    
    def transform(self, D):
        m, n = D.shape
        D -= self.L.mean(axis=0)
        R = np.cov(D, rowvar=False)
        evals, evecs = np.linalg.eigh(R)
        idx = np.argsort(evals)[::-1]
        self.evecs = evecs[:,idx]
        self.evals = evals[idx]
        print(self.n_components,self.evecs.shape,D.shape)
        if self.n_components is not None:
            self.evecs = evecs[:, :self.n_components]
        # carry out the transformation on the data using eigenvectors
        # and return the re-scaled data, eigenvalues, and eigenvectors
        return np.dot(self.evecs.T, D.T).T
    
    def inverse_transform(self, D):
        D = np.dot(D, self.evecs.T) + self.L.mean(axis=0)
        return D
    
    
    def plot_fit(self, size=None, tol=0.1, axis_on=True):

        n, d = self.D.shape

        if size:
            nrows, ncols = size
        else:
            sq = np.ceil(np.sqrt(n))
            nrows = int(sq)
            ncols = int(sq)

        ymin = np.nanmin(self.D)
        ymax = np.nanmax(self.D)
        print('ymin: {0}, ymax: {1}'.format(ymin, ymax))

        numplots = np.min([n, nrows * ncols])
        plt.figure()

        for n in range(numplots):
            plt.subplot(nrows, ncols, n + 1)
            plt.ylim((ymin - tol, ymax + tol))
            plt.plot(self.L[n, :] + self.S[n, :], 'r')
            plt.plot(self.L[n, :], 'b')
            if not axis_on:
                plt.axis('off')
    