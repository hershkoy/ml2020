"""
skTSNE: Wrapper file to allow adding TSNE into sklearn pipeline 
"""

# Author: Hezi Hershkovitz <hershkoy@gmail.com>
#         Guy Gurevich <guygu4@gmail.com>

from __future__ import division, print_function

import numpy as np
from sklearn.base import TransformerMixin,BaseEstimator
from sklearn.manifold import TSNE



class skTSNE(TransformerMixin, BaseEstimator):
    
    def __init__(self, **kw):
        #print(kw)
        self.tsne_model = TSNE(**kw)
        #print(self.tsne_model)
        
    def fit(self, X):
        self.tsne_model(X)
               
    def fit_transform(self, X, y=None):
        return self.tsne_model.fit_transform(X)
    
    def transform(self, X):
        return self.tsne_model.fit_transform(X)
